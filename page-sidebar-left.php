<!DOCTYPE HTML>
<html>

<?php
		
		include "includes/files/header_links.php";
		
		?>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        
		 <?php
		
		include "includes/files/page_header.php";
		
		?>

        <div class="container">
            <h1 class="page-title">Sidebar Left Page</h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <aside class="sidebar-left">
                        <div class="sidebar-widget">
                            <div class="Form">
                                <input class="form-control" placeholder="Search..." type="text" />
                            </div>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Categories</h4>
                            <ul class="icon-list list-category">
                                <li><a href="#"><i class="fa fa-angle-right"></i>Photos <small >(78)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Vacation <small >(73)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Flights <small >(90)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Travel Advices <small >(72)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Trending Now <small >(50)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Hotels <small >(91)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Places to Go <small >(76)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Travel Stories <small >(92)</small></a>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Popular Posts</h4>
                            <ul class="thumb-list">
                                <li>
                                    <a href="#">
                                        <img src="img/70x70.png" alt="Image Alternative text" title="Viva Las Vegas" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">Jul 18, 2014</p>
                                        <h5 class="thumb-list-item-title"><a href="#">Consequat nisl</a></h5>
                                        <p class="thumb-list-item-desciption">Nisl rhoncus cum pretium purus</p>
                                    </div>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/70x70.png" alt="Image Alternative text" title="4 Strokes of Fun" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">Jul 18, 2014</p>
                                        <h5 class="thumb-list-item-title"><a href="#">Ante volutpat</a></h5>
                                        <p class="thumb-list-item-desciption">Amet semper litora elit eu</p>
                                    </div>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/70x70.png" alt="Image Alternative text" title="Cup on red" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">Jul 18, 2014</p>
                                        <h5 class="thumb-list-item-title"><a href="#">Curabitur litora</a></h5>
                                        <p class="thumb-list-item-desciption">Ac penatibus non aenean tellus</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Twitter Feed</h4>
                            <div class="twitter" id="twitter"></div>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Recent Comments</h4>
                            <ul class="thumb-list thumb-list-right">
                                <li>
                                    <a href="#">
                                        <img class="rounded" src="img/70x70.png" alt="Image Alternative text" title="Afro" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">10 minutes ago</p>
                                        <h4 class="thumb-list-item-title"><a href="#">Joseph Watson</a></h4>
                                        <p class="thumb-list-item-desciption">Nascetur vehicula penatibus vel nisi ullamcorper nullam...</p>
                                    </div>
                                </li>
                                <li>
                                    <a href="#">
                                        <img class="rounded" src="img/70x70.png" alt="Image Alternative text" title="Gamer Chick" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">8 minutes ago</p>
                                        <h4 class="thumb-list-item-title"><a href="#">Joe Smith</a></h4>
                                        <p class="thumb-list-item-desciption">Malesuada commodo blandit quis porttitor sed class...</p>
                                    </div>
                                </li>
                                <li>
                                    <a href="#">
                                        <img class="rounded" src="img/70x70.png" alt="Image Alternative text" title="AMaze" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">5 minutes ago</p>
                                        <h4 class="thumb-list-item-title"><a href="#">Bernadette Cornish</a></h4>
                                        <p class="thumb-list-item-desciption">Sed convallis tincidunt laoreet dictumst natoque varius...</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Archive</h4>
                            <ul class="icon-list list-category">
                                <li><a href="#"><i class="fa fa-angle-right"></i>July 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>June 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>May 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>April 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>March 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>February 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>January 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>December 2014</a>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Gallery</h4>
                            <div class="row row-no-gutter">
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="The Big Showoff-Take 2" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="b and w camera" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="Me with the Uke" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="Good job" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="a dreamy jump" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="Happy Bokeh Day" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="sunny wood" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="4 Strokes of Fun" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="Spidy" />
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Facebook</h4>
                            <div class="fb-like-box" data-href="https://www.facebook.com/FacebookDevelopers" data-colorscheme="light" data-show-faces="1" data-header="1" data-show-border="1" data-width="233"></div>
                        </div>
                    </aside>
                </div>
                <div class="col-md-9">
                    <h3>Ad tortor nulla per rutrum faucibus class consectetur luctus sodales</h3>
                    <p>Vitae luctus leo vitae hac viverra mi pellentesque sollicitudin habitant at iaculis mi rutrum turpis blandit vulputate ad inceptos et gravida est id tincidunt cursus elit dignissim interdum mus diam placerat sed congue risus dis mollis ultrices est nec netus nec hac cursus ipsum dictum eu sed volutpat felis quam sociosqu risus blandit viverra consectetur quis bibendum porta vivamus imperdiet in congue est odio nulla vehicula feugiat viverra sollicitudin justo a in phasellus enim odio feugiat dolor pellentesque auctor quisque ad hac parturient curae donec nec eget magnis himenaeos adipiscing cras eleifend non ultricies egestas habitant massa vulputate sit fames curae libero scelerisque auctor curae ad luctus consectetur nisl turpis potenti interdum blandit sodales sodales justo porttitor amet praesent congue aliquet eget per venenatis dis non mus ut rhoncus ullamcorper posuere malesuada sociosqu habitasse vitae nullam accumsan pellentesque porttitor risus id aliquam proin urna mollis dolor interdum morbi natoque imperdiet nec nisl bibendum fames nec nam curae dapibus erat in integer posuere velit nascetur tortor cras metus ante elit dapibus facilisis felis molestie aenean conubia feugiat hac laoreet habitant mus semper nisl semper vestibulum malesuada erat taciti aliquet malesuada non fusce primis ipsum id netus mattis fringilla sapien libero habitasse</p>
                    <h4>Sollicitudin nam et primis arcu tristique tortor nisi mus</h4>
                    <p>Montes per laoreet laoreet porta lacus dui euismod bibendum metus imperdiet dui ultrices sapien varius scelerisque ultricies netus fringilla cras fermentum quam molestie sit sit nulla ad venenatis parturient aliquet purus convallis mi hac dis lacus penatibus ipsum feugiat ligula rutrum mus nostra facilisis condimentum nullam adipiscing nam habitant vestibulum aenean fermentum primis lacinia netus dictum venenatis fusce dictum eleifend etiam nam rhoncus augue imperdiet lacinia sit scelerisque massa risus felis a hendrerit vestibulum potenti rhoncus dapibus sociis viverra porta orci praesent nibh tempor ac lorem mauris ullamcorper vivamus laoreet duis tincidunt risus libero per integer consequat taciti fringilla morbi</p>
                    <p>Egestas mauris dictumst ultrices semper quam platea nisl condimentum ipsum senectus donec semper sollicitudin aliquet sem cursus tortor fermentum ad tellus turpis convallis ullamcorper curabitur praesent lectus commodo primis phasellus</p>
                    <h3>Potenti per semper nullam ultricies</h3>
                    <p>Donec cubilia turpis mollis semper ullamcorper himenaeos mi class suspendisse habitasse hac consequat dignissim porta a magnis nunc quis ultrices felis inceptos hendrerit blandit diam dui tellus vitae cursus euismod consectetur montes tempor gravida leo est interdum nisl porttitor consectetur habitant rutrum eget mus scelerisque commodo tempor mus habitant et vivamus neque class metus id non nulla natoque laoreet condimentum aliquam nunc vivamus aliquam ultrices justo habitasse taciti himenaeos cursus interdum natoque viverra nec maecenas nulla suspendisse tempus lacinia vivamus nec sagittis natoque consequat elit metus risus ac leo sed iaculis congue etiam sollicitudin dui inceptos fringilla himenaeos odio gravida potenti nisi magna turpis vulputate elit congue nascetur euismod molestie habitant eu dictumst diam montes molestie ridiculus inceptos montes etiam orci ullamcorper platea volutpat tortor varius sit orci conubia dolor facilisi platea lacinia ullamcorper vehicula consequat id sapien consectetur imperdiet commodo at ornare litora cursus lectus torquent senectus per cum placerat lacinia duis torquent id quam torquent turpis semper quis facilisi tristique fringilla ultrices mattis commodo ridiculus dolor cubilia dui magna bibendum eu lobortis fermentum mauris mauris neque nostra viverra vehicula ridiculus donec libero cursus quam leo id eros quam diam purus sociosqu luctus pulvinar ad vivamus curae vivamus cursus</p>
                    <p>Etiam mattis elit ornare sociosqu quam himenaeos leo elit nisi posuere adipiscing nisi est commodo habitant magna maecenas tincidunt tincidunt ac lobortis ullamcorper nostra quam urna metus vulputate semper commodo nam a sagittis phasellus himenaeos class ridiculus tristique scelerisque enim habitasse maecenas dictum ut nullam orci himenaeos consequat dictumst class porttitor est conubia arcu aenean habitasse amet erat placerat sodales aenean turpis mauris posuere pharetra primis tempor ipsum augue ut nisi urna sociis enim aenean pretium aenean torquent accumsan rhoncus eros nec torquent mi nullam mi mattis vitae potenti litora duis natoque nulla nibh sollicitudin facilisi ut ornare quam non quisque sociosqu ridiculus maecenas placerat torquent enim imperdiet sed ad aenean euismod posuere ut ligula pellentesque magnis tellus iaculis litora quis mollis conubia dapibus neque curae mus quam natoque leo ac pharetra senectus nulla fermentum ullamcorper ullamcorper integer sollicitudin ultricies velit ad lacinia placerat molestie dapibus inceptos penatibus pulvinar blandit nostra sit dapibus magna interdum ornare ultrices nibh odio blandit neque consectetur nunc nulla enim etiam elit dui metus sapien semper fusce luctus tincidunt dignissim velit primis etiam potenti montes hac dictum egestas auctor venenatis felis auctor vulputate odio tristique class facilisis vestibulum pellentesque odio lectus arcu sed montes in</p>
                    <p>Malesuada arcu cursus nullam id odio fusce condimentum fames eros integer fames habitasse cum dui dictumst convallis ad condimentum egestas pretium class ultricies id primis bibendum ad cras elementum dui egestas leo suscipit dis tincidunt sagittis pellentesque eros suscipit convallis enim consequat lobortis ut pretium volutpat nec dictum per nisi mattis fermentum dictumst curabitur convallis gravida taciti iaculis rhoncus porttitor scelerisque magna et iaculis neque dui viverra sit faucibus quisque quam aenean nec suscipit turpis lectus lobortis potenti faucibus amet netus ante facilisis facilisis aenean blandit potenti dictum iaculis morbi tortor cum proin ornare porta dictum placerat condimentum ligula pulvinar fusce cras ad scelerisque et litora pretium senectus netus fames lacinia rutrum tempus iaculis ligula cum elementum tristique per tincidunt eros nibh inceptos lobortis aliquet pharetra magnis suspendisse dapibus tellus facilisis mi pulvinar imperdiet pellentesque leo habitant vehicula ullamcorper bibendum bibendum ridiculus ullamcorper integer morbi nec volutpat tempor sit netus vulputate lorem metus mauris etiam penatibus pretium neque per ipsum adipiscing massa metus dignissim mauris massa phasellus vestibulum sapien fames luctus dui velit consectetur ridiculus torquent netus parturient tortor lorem sapien cum porttitor ut curae faucibus mi proin risus hendrerit duis sodales enim adipiscing consequat primis penatibus massa auctor hendrerit</p>
                    <h3>Nostra mollis justo duis dolor</h3>
                    <p>Fringilla accumsan erat ac non dictumst vel convallis neque rutrum est dapibus viverra curae pharetra dui ad cum cum suscipit ipsum class cubilia dignissim id sodales odio semper ornare proin condimentum senectus lacinia nullam malesuada scelerisque laoreet sodales fames sollicitudin tortor elementum luctus mus mi curae magnis adipiscing morbi leo fermentum sociosqu pharetra lectus lacus class convallis viverra sagittis imperdiet nunc lacinia suspendisse semper in non nostra vestibulum cras tempor mi ut bibendum auctor nam felis vehicula mollis dignissim dolor vel consequat iaculis dignissim semper curae nibh himenaeos quis malesuada sem fusce magna magnis magna dictumst eleifend vitae non massa potenti euismod nunc inceptos cubilia etiam metus libero elit euismod quisque quisque eleifend dapibus fames ultrices sociis erat nunc adipiscing enim inceptos posuere pellentesque egestas ut vulputate urna vel posuere lorem lacinia cursus auctor nisl elementum bibendum suspendisse hac venenatis ipsum justo dolor platea imperdiet nibh congue adipiscing praesent primis montes ante nullam penatibus luctus hac sit quam id ante montes senectus dui praesent magnis eget ligula libero torquent semper at sollicitudin litora ultricies metus a justo in sagittis malesuada maecenas nec fringilla leo tempor faucibus conubia lacus faucibus varius odio montes accumsan faucibus sociis id porta quisque dictum aliquet nec cum parturient congue id scelerisque praesent placerat feugiat natoque gravida hac conubia sapien vulputate egestas a feugiat ornare tempus per massa rhoncus ipsum habitasse dapibus fermentum lacinia ridiculus pharetra rutrum ac cubilia suscipit consectetur purus dictum aptent sagittis pulvinar condimentum consequat ridiculus netus odio in molestie iaculis a pretium</p>
                </div>
            </div>
        </div>



        <div class="gap"></div>
        
		<?php
		
		include "includes/files/footer.php";
		
		?>


        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script src="js/nicescroll.js"></script>
        <script src="js/dropit.js"></script>
        <script src="js/ionrangeslider.js"></script>
        <script src="js/icheck.js"></script>
        <script src="js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="js/typeahead.js"></script>
        <script src="js/card-payment.js"></script>
        <script src="js/magnific.js"></script>
        <script src="js/owl-carousel.js"></script>
        <script src="js/fitvids.js"></script>
        <script src="js/tweet.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/gridrotator.js"></script>
        <script src="js/custom.js"></script>
    </div>
</body>

</html>


