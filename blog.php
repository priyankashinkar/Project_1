<!DOCTYPE HTML>
<html>

<?php
		
		include "includes/files/header_links.php";
		
		?>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        
		 <?php
		
		include "includes/files/page_header.php";
		
		?>

        <div class="container">
            <h1 class="page-title">Traveler Blog</h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <!-- START BLOG POST -->
                    <div class="article post">
                        <header class="post-header">
                            <a class="hover-img" href="#">
                                <img src="img/1200x500.png" alt="Image Alternative text" title="196_365" /><i class="fa fa-link box-icon-# hover-icon round"></i>
                            </a>
                        </header>
                        <div class="post-inner">
                            <h4 class="post-title"><a class="text-darken" href="#">Image Post Type</a></h4>
                            <ul class="post-meta">
                                <li><i class="fa fa-calendar"></i><a href="#">14 October, 2014</a>
                                </li>
                                <li><i class="fa fa-user"></i><a href="#">Blake Abraham</a>
                                </li>
                                <li><i class="fa fa-tags"></i><a href="#">Travel</a>
                                </li>
                                <li><i class="fa fa-comments"></i><a href="#">0 Comments</a>
                                </li>
                            </ul>
                            <p class="post-desciption">Porttitor feugiat mus cras quisque pharetra sagittis non laoreet augue nulla lectus auctor accumsan cubilia sollicitudin mattis leo vel morbi class sollicitudin cubilia quisque penatibus dictumst faucibus dui natoque ultricies montes congue pellentesque aliquet lectus dictum est volutpat class odio elementum quis commodo dolor ultrices scelerisque montes class curabitur class</p><a class="btn btn-small btn-primary" href="#">Read More</a>
                        </div>
                    </div>
                    <!-- END BLOG POST -->
                    <!-- START BLOG POST -->
                    <div class="article post">
                        <header class="post-header">
                            <blockquote>Lorem in sollicitudin eleifend mattis nec ad euismod ad magna ad enim commodo hac porta bibendum fusce tortor inceptos erat</blockquote>
                        </header>
                        <div class="post-inner">
                            <h4 class="post-title"><a class="text-darken" href="#">Quoute Post Type</a></h4>
                            <ul class="post-meta">
                                <li><i class="fa fa-calendar"></i><a href="#">12 October, 2014</a>
                                </li>
                                <li><i class="fa fa-user"></i><a href="#">Bernadette Cornish</a>
                                </li>
                                <li><i class="fa fa-tags"></i><a href="#">Travel</a>, <a href="#">Lifestyle</a>
                                </li>
                                <li><i class="fa fa-comments"></i><a href="#">18 Comments</a>
                                </li>
                            </ul>
                            <p class="post-desciption">Sociosqu fermentum venenatis pulvinar dictum hac pulvinar a lobortis duis nascetur ad facilisis nullam sagittis elit quis sapien enim pellentesque laoreet lectus facilisis pretium laoreet sapien risus justo habitant nascetur adipiscing dolor est vivamus tincidunt amet rutrum purus posuere ipsum vulputate class fringilla euismod magnis primis condimentum conubia iaculis tincidunt</p><a class="btn btn-small btn-primary" href="#">Read More</a>
                        </div>
                    </div>
                    <!-- END BLOG POST -->
                    <!-- START BLOG POST -->
                    <div class="article post">
                        <header class="post-header">
                            <div class="fotorama" data-allowfullscreen="true">
                                <img src="img/1200x500.png" alt="Image Alternative text" title="196_365" />
                                <img src="img/1200x500.png" alt="Image Alternative text" title="196_365" />
                                <img src="img/1200x500.png" alt="Image Alternative text" title="196_365" />
                            </div>
                        </header>
                        <div class="post-inner">
                            <h4 class="post-title"><a class="text-darken" href="#">Slider Post Type</a></h4>
                            <ul class="post-meta">
                                <li><i class="fa fa-calendar"></i><a href="#">01 October, 2014</a>
                                </li>
                                <li><i class="fa fa-user"></i><a href="#">Frank Mills</a>
                                </li>
                                <li><i class="fa fa-tags"></i><a href="#">Web</a>, <a href="#">Digital</a>, <a href="#">Typography</a>
                                </li>
                                <li><i class="fa fa-comments"></i><a href="#">17 Comments</a>
                                </li>
                            </ul>
                            <p class="post-desciption">Suscipit ad inceptos vitae vulputate facilisis elementum facilisi at potenti magnis molestie integer potenti erat ullamcorper suspendisse etiam consequat curabitur risus taciti diam litora vulputate ligula ad natoque suspendisse mattis pulvinar porta dui ac ut morbi integer integer porttitor habitasse et per nunc sociis curae eu semper dis ullamcorper dolor</p><a class="btn btn-small btn-primary" href="#">Read More</a>
                        </div>
                    </div>
                    <!-- END BLOG POST -->
                    <!-- START BLOG POST -->
                    <div class="article post">
                        <header class="post-header"><a class="post-link" href="#">Google.com</a>
                        </header>
                        <div class="post-inner">
                            <h4 class="post-title"><a class="text-darken" href="#">Link Post Type</a></h4>
                            <ul class="post-meta">
                                <li><i class="fa fa-calendar"></i><a href="#">10 September, 2014</a>
                                </li>
                                <li><i class="fa fa-user"></i><a href="#">Oliver Ross</a>
                                </li>
                                <li><i class="fa fa-tags"></i><a href="#">Typography</a>
                                </li>
                                <li><i class="fa fa-comments"></i><a href="#">18 Comments</a>
                                </li>
                            </ul>
                            <p class="post-desciption">Vel aptent gravida semper suscipit fusce curae conubia aliquet ullamcorper consectetur integer nisl ac at nostra cum feugiat habitant eget quis condimentum sed duis aenean ante sagittis amet aptent id cras placerat laoreet senectus dignissim nisl erat netus scelerisque ridiculus congue inceptos fames pellentesque commodo lorem morbi eget semper molestie</p><a class="btn btn-small btn-primary" href="#">Read More</a>
                        </div>
                    </div>
                    <!-- END BLOG POST -->
                    <!-- START BLOG POST -->
                    <div class="article post">
                        <header class="post-header">
                            <iframe src="//www.youtube.com/embed/6iHwPfirtUg" frameborder="0" allowfullscreen></iframe>
                        </header>
                        <div class="post-inner">
                            <h4 class="post-title"><a class="text-darken" href="#">Youtube Post Type</a></h4>
                            <ul class="post-meta">
                                <li><i class="fa fa-calendar"></i><a href="#">07 August, 2014</a>
                                </li>
                                <li><i class="fa fa-user"></i><a href="#">Bernadette Cornish</a>
                                </li>
                                <li><i class="fa fa-tags"></i><a href="#">Design</a>, <a href="#">Travel</a>
                                </li>
                                <li><i class="fa fa-comments"></i><a href="#">14 Comments</a>
                                </li>
                            </ul>
                            <p class="post-desciption">Sagittis mus faucibus habitasse sodales eros ultricies rhoncus ultrices faucibus sapien at suspendisse per mauris vitae accumsan sed dictum potenti dui porta id volutpat sem scelerisque dui aliquam parturient blandit et nam augue aenean ornare sagittis ultricies nisi ut laoreet morbi erat eget pretium rutrum vitae consequat hendrerit viverra duis</p><a class="btn btn-small btn-primary" href="#">Read More</a>
                        </div>
                    </div>
                    <!-- END BLOG POST -->
                    <!-- START BLOG POST -->
                    <div class="article post">
                        <header class="post-header">
                            <iframe src="http://player.vimeo.com/video/103721959" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                        </header>
                        <div class="post-inner">
                            <h4 class="post-title"><a class="text-darken" href="#">Vimeo Post Type</a></h4>
                            <ul class="post-meta">
                                <li><i class="fa fa-calendar"></i><a href="#">21 May, 2014</a>
                                </li>
                                <li><i class="fa fa-user"></i><a href="#">Joseph Watson</a>
                                </li>
                                <li><i class="fa fa-tags"></i><a href="#">Typography</a>, <a href="#">Travel</a>
                                </li>
                                <li><i class="fa fa-comments"></i><a href="#">8 Comments</a>
                                </li>
                            </ul>
                            <p class="post-desciption">Non fringilla conubia consectetur curabitur bibendum semper laoreet sociis class enim leo eget blandit rhoncus nisl libero eu dui torquent bibendum integer urna rhoncus duis pellentesque quisque donec ligula rhoncus ante pharetra tincidunt velit orci cras condimentum varius tempus at dolor ut nostra laoreet ridiculus per diam posuere ullamcorper habitant</p><a class="btn btn-small btn-primary" href="#">Read More</a>
                        </div>
                    </div>
                    <!-- END BLOG POST -->
                    <!-- START BLOG POST -->
                    <div class="article post">
                        <header class="post-header">
                            <iframe width="100%" height="150" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/150793348&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=false"></iframe>
                        </header>
                        <div class="post-inner">
                            <h4 class="post-title"><a class="text-darken" href="#">Audio Post Type</a></h4>
                            <ul class="post-meta">
                                <li><i class="fa fa-calendar"></i><a href="#">21 December, 2013</a>
                                </li>
                                <li><i class="fa fa-user"></i><a href="#">Neil Davidson</a>
                                </li>
                                <li><i class="fa fa-tags"></i><a href="#">Digital</a>
                                </li>
                                <li><i class="fa fa-comments"></i><a href="#">4 Comments</a>
                                </li>
                            </ul>
                            <p class="post-desciption">A placerat primis tellus sem rutrum posuere class eleifend ut euismod pharetra nulla nullam nascetur sociis sed sed conubia eu cras velit at enim litora amet viverra leo consequat molestie vestibulum class velit inceptos nostra ullamcorper faucibus commodo ad consequat aliquam nostra rhoncus laoreet lobortis ipsum venenatis augue libero libero</p><a class="btn btn-small btn-primary" href="#">Read More</a>
                        </div>
                    </div>
                    <!-- END BLOG POST -->
                    <!-- START BLOG POST -->
                    <div class="article post">
                        <div class="post-inner">
                            <h4 class="post-title"><a class="text-darken" href="#">Default Post Type</a></h4>
                            <ul class="post-meta">
                                <li><i class="fa fa-calendar"></i><a href="#">23 February, 2013</a>
                                </li>
                                <li><i class="fa fa-user"></i><a href="#">Sarah Slater</a>
                                </li>
                                <li><i class="fa fa-tags"></i><a href="#">Web</a>, <a href="#">Travel</a>, <a href="#">Digital</a>
                                </li>
                                <li><i class="fa fa-comments"></i><a href="#">1 Comments</a>
                                </li>
                            </ul>
                            <p class="post-desciption">Ornare conubia tortor gravida lacus arcu sem cum platea pretium nascetur suspendisse metus gravida vulputate lectus mattis nunc lectus hendrerit quis sem quisque nisi suscipit nibh eu nostra torquent dolor suspendisse iaculis lobortis hac sociosqu habitasse pharetra etiam ante mauris congue phasellus sollicitudin fringilla suspendisse risus auctor risus metus fames</p><a class="btn btn-small btn-primary" href="#">Read More</a>
                        </div>
                    </div>
                    <!-- END BLOG POST -->
                    <ul class="pagination">
                        <li class="active"><a href="#">1</a>
                        </li>
                        <li><a href="#">2</a>
                        </li>
                        <li><a href="#">3</a>
                        </li>
                        <li><a href="#">4</a>
                        </li>
                        <li><a href="#">5</a>
                        </li>
                        <li><a href="#">6</a>
                        </li>
                        <li><a href="#">7</a>
                        </li>
                        <li class="dots">...</li>
                        <li><a href="#">43</a>
                        </li>
                        <li class="next"><a href="#">Next Page</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <aside class="sidebar-right">
                        <div class="sidebar-widget">
                            <div class="Form">
                                <input class="form-control" placeholder="Search..." type="text" />
                            </div>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Categories</h4>
                            <ul class="icon-list list-category">
                                <li><a href="#"><i class="fa fa-angle-right"></i>Photos <small >(63)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Vacation <small >(81)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Flights <small >(83)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Travel Advices <small >(95)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Trending Now <small >(57)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Hotels <small >(94)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Places to Go <small >(81)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Travel Stories <small >(91)</small></a>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Popular Posts</h4>
                            <ul class="thumb-list">
                                <li>
                                    <a href="#">
                                        <img src="img/70x70.png" alt="Image Alternative text" title="Viva Las Vegas" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">Jul 18, 2014</p>
                                        <h5 class="thumb-list-item-title"><a href="#">Rhoncus fusce</a></h5>
                                        <p class="thumb-list-item-desciption">Semper suspendisse varius ligula habitasse</p>
                                    </div>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/70x70.png" alt="Image Alternative text" title="4 Strokes of Fun" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">Jul 18, 2014</p>
                                        <h5 class="thumb-list-item-title"><a href="#">Montes non</a></h5>
                                        <p class="thumb-list-item-desciption">Accumsan hendrerit velit congue lobortis</p>
                                    </div>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/70x70.png" alt="Image Alternative text" title="Cup on red" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">Jul 18, 2014</p>
                                        <h5 class="thumb-list-item-title"><a href="#">Varius feugiat</a></h5>
                                        <p class="thumb-list-item-desciption">Euismod scelerisque phasellus hendrerit magna</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Twitter Feed</h4>
                            <div class="twitter" id="twitter"></div>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Recent Comments</h4>
                            <ul class="thumb-list thumb-list-right">
                                <li>
                                    <a href="#">
                                        <img class="rounded" src="img/70x70.png" alt="Image Alternative text" title="Afro" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">7 minutes ago</p>
                                        <h4 class="thumb-list-item-title"><a href="#">Alison Mackenzie</a></h4>
                                        <p class="thumb-list-item-desciption">Himenaeos cras platea aliquet consequat justo congue...</p>
                                    </div>
                                </li>
                                <li>
                                    <a href="#">
                                        <img class="rounded" src="img/70x70.png" alt="Image Alternative text" title="AMaze" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">6 minutes ago</p>
                                        <h4 class="thumb-list-item-title"><a href="#">Blake Hardacre</a></h4>
                                        <p class="thumb-list-item-desciption">Sit gravida augue neque consequat elementum ultrices...</p>
                                    </div>
                                </li>
                                <li>
                                    <a href="#">
                                        <img class="rounded" src="img/70x70.png" alt="Image Alternative text" title="Gamer Chick" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">9 minutes ago</p>
                                        <h4 class="thumb-list-item-title"><a href="#">Bernadette Cornish</a></h4>
                                        <p class="thumb-list-item-desciption">Pellentesque lacus sociosqu ac malesuada urna condimentum...</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Archive</h4>
                            <ul class="icon-list list-category">
                                <li><a href="#"><i class="fa fa-angle-right"></i>July 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>June 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>May 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>April 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>March 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>February 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>January 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>December 2014</a>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Gallery</h4>
                            <div class="row row-no-gutter">
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="The Big Showoff-Take 2" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="Good job" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="4 Strokes of Fun" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="b and w camera" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="Happy Bokeh Day" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="sunny wood" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="a dreamy jump" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="Spidy" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="Me with the Uke" />
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Facebook</h4>
                            <div class="fb-like-box" data-href="https://www.facebook.com/FacebookDevelopers" data-colorscheme="light" data-show-faces="1" data-header="1" data-show-border="1" data-width="233"></div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>



        <div class="gap"></div>
        
		<?php
		
		include "includes/files/footer.php";
		
		?>


        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script src="js/nicescroll.js"></script>
        <script src="js/dropit.js"></script>
        <script src="js/ionrangeslider.js"></script>
        <script src="js/icheck.js"></script>
        <script src="js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="js/typeahead.js"></script>
        <script src="js/card-payment.js"></script>
        <script src="js/magnific.js"></script>
        <script src="js/owl-carousel.js"></script>
        <script src="js/fitvids.js"></script>
        <script src="js/tweet.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/gridrotator.js"></script>
        <script src="js/custom.js"></script>
    </div>
</body>

</html>


