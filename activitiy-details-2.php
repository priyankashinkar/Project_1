<!DOCTYPE HTML>
<html>

<?php
		
		include "includes/files/header_links.php";
		
		?>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        
		 <?php
		
		include "includes/files/page_header.php";
		
		?>





        <div class="container">
            <ul class="breadcrumb">
                <li><a href="index.html">Home</a>
                </li>
                <li><a href="#">United States</a>
                </li>
                <li><a href="#">New York (NY)</a>
                </li>
                <li><a href="#">New York City</a>
                </li>
                <li><a href="#">New York Activities</a>
                </li>
                <li class="active">Central Park Trip</li>
            </ul>
            <div class="booking-item-details">
                <header class="booking-item-header">
                    <div class="row">
                        <div class="col-md-9">
                            <h2 class="lh1em">Central Park Trip</h2>
                            <p class="lh1em text-small"><i class="fa fa-map-marker"></i> 6782 Sarasea Circle, Siesta Key, FL 34242</p>
                            <ul class="list list-inline text-small">
                                <li><a href="#"><i class="fa fa-envelope"></i> Owner E-mail</a>
                                </li>
                                <li><a href="#"><i class="fa fa-home"></i> Owner Website</a>
                                </li>
                                <li><i class="fa fa-phone"></i> +1 (453) 458-5628</li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <p class="booking-item-header-price"><small>price</small>  <span class="text-lg">Free</span>
                            </p>
                        </div>
                    </div>
                </header>
                <div class="row">
                    <div class="col-md-7">
                        <div class="tabbable booking-details-tabbable">
                            <ul class="nav nav-tabs" id="myTab">
                                <li class="active"><a href="#tab-1" data-toggle="tab"><i class="fa fa-camera"></i>Photos</a>
                                </li>
                                <li><a href="#google-map-tab" data-toggle="tab"><i class="fa fa-map-marker"></i>On the Map</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab-1">
                                    <!-- START LIGHTBOX GALLERY -->
                                    <div class="row row-no-gutter" id="popup-gallery">
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="AMaze" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="Trebbiano Ristorante - japenese breakfast" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="4 Strokes of Fun" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="The Big Showoff-Take 2" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="Upper Lake in New York Central Park" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="Old No7" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="Me with the Uke" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="Plunklock live in Cologne" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="Pictures at the museum" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="new york at an angle" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="Upper Lake in New York Central Park" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="Plunklock live in Cologne" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="new york at an angle" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="Plunklock live in Cologne" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="Upper Lake in New York Central Park" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="Me with the Uke" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- END LIGHTBOX GALLERY -->
                                </div>
                                <div class="tab-pane fade" id="google-map-tab">
                                    <div id="map-canvas" style="width:100%; height:500px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="booking-item-meta">
                            <h2 class="lh1em mt40">Exeptional!</h2>
                            <h3>97% <small >of guests recommend</small></h3>
                            <div class="booking-item-rating">
                                <ul class="icon-list icon-group booking-item-rating-stars">
                                    <li><i class="fa fa-star"></i>
                                    </li>
                                    <li><i class="fa fa-star"></i>
                                    </li>
                                    <li><i class="fa fa-star"></i>
                                    </li>
                                    <li><i class="fa fa-star"></i>
                                    </li>
                                    <li><i class="fa fa-star"></i>
                                    </li>
                                </ul><span class="booking-item-rating-number"><b >4.7</b> of 5 <small class="text-smaller">guest rating</small></span>
                                <p><a class="text-default" href="#">based on 1535 reviews</a>
                                </p>
                            </div>
                        </div>
                        <div class="gap gap-small">
                            <h3>Owner description</h3>
                            <p>Pretium pharetra ipsum fermentum suscipit magna fringilla congue non dui penatibus per libero varius odio sodales hac et vestibulum vivamus sapien facilisis tempor ullamcorper primis non augue a nunc facilisis sapien auctor litora vestibulum purus luctus posuere porttitor cubilia at felis commodo potenti diam ridiculus enim per orci aliquet quam</p>
                        </div>
                        <a href="#" class="btn btn-primary btn-lg">Add to Trip</a>
                    </div>
                </div>
            </div>
            <div class="gap"></div>
            <h3 class="mb20">Activity Reviews</h3>
            <div class="row row-wrap">
                <div class="col-md-8">
                    <ul class="booking-item-reviews list">
                        <li>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="booking-item-review-person">
                                        <a class="booking-item-review-person-avatar round" href="#">
                                            <img src="img/70x70.png" alt="Image Alternative text" title="Good job" />
                                        </a>
                                        <p class="booking-item-review-person-name"><a href="#">John Doe</a>
                                        </p>
                                        <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">91 Reviews</a></small>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="booking-item-review-content">
                                        <h5>"Etiam aliquam posuere duis a"</h5>
                                        <ul class="icon-group booking-item-rating-stars">
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                        </ul>
                                        <p>Fringilla enim dictum tortor accumsan litora faucibus felis mi lacinia sociis commodo suscipit pulvinar ligula nibh litora malesuada ipsum orci<span class="booking-item-review-more"> Felis facilisi interdum neque torquent egestas pharetra est cum tellus ultrices aliquam nam gravida hendrerit primis class egestas primis porta egestas non eleifend risus turpis commodo nisi felis nullam risus aliquam curae fusce elit est ornare metus nibh mauris nunc cras ipsum magnis</span>
                                        </p>
                                        <div class="booking-item-review-more-content">
                                            <p>Lectus pellentesque sed lacinia ut aptent hac aenean leo sagittis dapibus mauris sit sociosqu sit</p>
                                            <p>Non ipsum penatibus lacus molestie aptent elementum nascetur a blandit aenean fusce eleifend hendrerit ac fringilla vehicula eget odio orci hac mauris tincidunt tellus dis nec eleifend commodo per enim orci urna malesuada tincidunt nec libero turpis fermentum accumsan est non tristique interdum ligula sit luctus purus nulla et imperdiet maecenas suspendisse diam lorem nisi quis elit augue mus interdum porttitor ante fermentum interdum est primis hendrerit</p>
                                            <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <ul class="list booking-item-raiting-summary-list">
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Sleep</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Location</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Service</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <ul class="list booking-item-raiting-summary-list">
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Clearness</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Rooms</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                        </div>
                                        <p class="booking-item-review-rate">Was this review helpful?
                                            <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 18</b>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="booking-item-review-person">
                                        <a class="booking-item-review-person-avatar round" href="#">
                                            <img src="img/70x70.png" alt="Image Alternative text" title="AMaze" />
                                        </a>
                                        <p class="booking-item-review-person-name"><a href="#">Minnie Aviles</a>
                                        </p>
                                        <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">27 Reviews</a></small>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="booking-item-review-content">
                                        <h5>"Suscipit curae venenatis"</h5>
                                        <ul class="icon-group booking-item-rating-stars">
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                        </ul>
                                        <p>Pellentesque fusce class class per vel facilisi vestibulum hac euismod luctus ultrices dui leo suspendisse sed dictumst ad cursus velit quam blandit nisl egestas tempor eros turpis vel aenean nunc eu viverra dictum sagittis libero pellentesque scelerisque senectus odio<span class="booking-item-review-more"> Sit suscipit fringilla mus sit sem interdum justo massa orci parturient velit donec semper cum imperdiet ridiculus facilisis magnis justo morbi nascetur rutrum etiam iaculis nec non placerat nisi sed at suscipit at duis quam massa consectetur euismod quam suspendisse netus vivamus penatibus nunc sociosqu quisque neque amet porttitor nisl commodo</span>
                                        </p>
                                        <div class="booking-item-review-more-content">
                                            <p>Sem metus vivamus faucibus curabitur cum sollicitudin sed sapien ultrices placerat placerat vehicula phasellus litora mollis fringilla ridiculus odio litora dapibus amet dignissim ante diam sociosqu neque imperdiet natoque a rutrum augue augue</p>
                                            <p>Tellus est dui tristique euismod vulputate id risus non leo phasellus ullamcorper penatibus nec posuere aliquam sed taciti morbi habitasse tempor et sem nisi duis etiam natoque vivamus pharetra sociosqu lacus dui netus lacus maecenas mi sem ad feugiat odio pharetra augue ante</p>
                                            <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <ul class="list booking-item-raiting-summary-list">
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Sleep</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Location</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Service</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <ul class="list booking-item-raiting-summary-list">
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Clearness</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o text-gray"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Rooms</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                        </div>
                                        <p class="booking-item-review-rate">Was this review helpful?
                                            <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 16</b>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="booking-item-review-person">
                                        <a class="booking-item-review-person-avatar round" href="#">
                                            <img src="img/70x70.png" alt="Image Alternative text" title="Afro" />
                                        </a>
                                        <p class="booking-item-review-person-name"><a href="#">Cyndy Naquin</a>
                                        </p>
                                        <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">138 Reviews</a></small>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="booking-item-review-content">
                                        <h5>"Quis aenean nascetur"</h5>
                                        <ul class="icon-group booking-item-rating-stars">
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                        </ul>
                                        <p>Enim nostra duis venenatis ad justo bibendum eleifend vehicula torquent vestibulum sodales conubia erat pretium rhoncus cursus eget placerat lobortis cras aenean porttitor ultrices<span class="booking-item-review-more"> Parturient himenaeos consectetur neque scelerisque lorem rhoncus laoreet dapibus at a varius suscipit habitasse penatibus auctor ligula at commodo euismod arcu ipsum cum natoque est consequat curae id cursus integer vestibulum platea habitant vivamus urna venenatis ultrices at tempor diam ligula in curae sagittis at cursus nulla risus gravida congue nisl non himenaeos lectus vehicula enim purus</span>
                                        </p>
                                        <div class="booking-item-review-more-content">
                                            <p>Tempus ac dapibus nascetur ornare nostra fusce lacinia litora consequat egestas ipsum mollis hendrerit sapien conubia nibh</p>
                                            <p>Porta mauris pellentesque suscipit vel hendrerit nisi aliquam luctus sed ante habitant conubia sed nisi metus eget sociis gravida ridiculus nunc suspendisse tellus senectus placerat habitasse eu tempus eget eu dapibus per nisl consectetur nulla ipsum pellentesque senectus eleifend sit proin tellus congue convallis pharetra eget sit aliquet senectus himenaeos magnis ridiculus aenean erat porta aptent nullam hac mi commodo litora ornare neque nulla tincidunt nibh venenatis sollicitudin pulvinar</p>
                                            <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <ul class="list booking-item-raiting-summary-list">
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Sleep</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Location</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Service</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o text-gray"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <ul class="list booking-item-raiting-summary-list">
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Clearness</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Rooms</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                        </div>
                                        <p class="booking-item-review-rate">Was this review helpful?
                                            <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 18</b>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="booking-item-review-person">
                                        <a class="booking-item-review-person-avatar round" href="#">
                                            <img src="img/70x70.png" alt="Image Alternative text" title="Me with the Uke" />
                                        </a>
                                        <p class="booking-item-review-person-name"><a href="#">Carol Blevins</a>
                                        </p>
                                        <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">105 Reviews</a></small>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="booking-item-review-content">
                                        <h5>"Curae elit magnis hac semper mollis rutrum"</h5>
                                        <ul class="icon-group booking-item-rating-stars">
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                        </ul>
                                        <p>Nisi molestie laoreet lacinia sapien rutrum adipiscing ornare sapien mi torquent cursus varius elementum cum dignissim augue torquent iaculis libero montes varius pharetra cursus dis himenaeos nunc amet hendrerit odio<span class="booking-item-review-more"> Habitant felis tortor potenti ipsum platea cubilia auctor ultrices inceptos ut pharetra lobortis cubilia nunc a cubilia curae at ac montes curabitur neque dapibus quisque habitant adipiscing libero in litora pulvinar fringilla molestie praesent natoque rhoncus rhoncus sociis taciti eleifend</span>
                                        </p>
                                        <div class="booking-item-review-more-content">
                                            <p>Dis adipiscing ligula porttitor ipsum a porta nunc mattis a lacinia vulputate blandit primis volutpat est eget interdum eget ipsum mi</p>
                                            <p>Quisque magnis tortor blandit a nulla porta magnis sollicitudin velit et sollicitudin mauris habitant nisi pulvinar proin cras cras erat praesent cursus taciti a dictum ridiculus elit at habitant sem enim aliquet urna nulla gravida adipiscing curabitur ante malesuada nisl</p>
                                            <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <ul class="list booking-item-raiting-summary-list">
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Sleep</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o text-gray"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Location</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Service</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o text-gray"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <ul class="list booking-item-raiting-summary-list">
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Clearness</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Rooms</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                        </div>
                                        <p class="booking-item-review-rate">Was this review helpful?
                                            <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 16</b>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="booking-item-review-person">
                                        <a class="booking-item-review-person-avatar round" href="#">
                                            <img src="img/70x70.png" alt="Image Alternative text" title="Chiara" />
                                        </a>
                                        <p class="booking-item-review-person-name"><a href="#">Cheryl Gustin</a>
                                        </p>
                                        <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">97 Reviews</a></small>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="booking-item-review-content">
                                        <h5>"Proin lacus dictumst tempor torquent nulla"</h5>
                                        <ul class="icon-group booking-item-rating-stars">
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                        </ul>
                                        <p>Himenaeos laoreet gravida vel mauris iaculis faucibus etiam nisl facilisi massa ut class etiam curae rhoncus velit ultricies vivamus conubia penatibus feugiat dictumst sollicitudin dictum bibendum<span class="booking-item-review-more"> Malesuada etiam vitae etiam cubilia eros ad mattis sit cubilia risus tempor risus quisque egestas venenatis potenti lobortis senectus tellus sodales commodo montes dictumst tortor a suspendisse accumsan blandit adipiscing proin ut cubilia malesuada odio aptent est etiam mollis velit dictumst posuere turpis purus consequat nullam potenti ac sagittis iaculis justo sociis interdum sed a senectus porta erat ultricies auctor tellus natoque cursus cursus</span>
                                        </p>
                                        <div class="booking-item-review-more-content">
                                            <p>Parturient commodo diam donec est pharetra ut rutrum justo ornare ullamcorper volutpat fusce taciti augue justo pulvinar pellentesque tortor dapibus sodales tincidunt fusce sociis hendrerit tempus tellus malesuada mi magnis litora ridiculus amet at netus primis cras lectus donec vivamus mi venenatis sapien non lacus lorem fermentum</p>
                                            <p>Dui nascetur fermentum elit tincidunt curabitur gravida morbi duis turpis tincidunt platea faucibus aptent sapien orci quam faucibus ipsum tempor nunc tincidunt netus habitasse mus mattis parturient mus nisl inceptos litora laoreet id est habitasse lorem vitae gravida diam nunc</p>
                                            <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <ul class="list booking-item-raiting-summary-list">
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Sleep</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Location</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Service</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <ul class="list booking-item-raiting-summary-list">
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Clearness</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Rooms</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                        </div>
                                        <p class="booking-item-review-rate">Was this review helpful?
                                            <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 8</b>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="booking-item-review-person">
                                        <a class="booking-item-review-person-avatar round" href="#">
                                            <img src="img/70x70.png" alt="Image Alternative text" title="Bubbles" />
                                        </a>
                                        <p class="booking-item-review-person-name"><a href="#">Joe Smith</a>
                                        </p>
                                        <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">118 Reviews</a></small>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="booking-item-review-content">
                                        <h5>"Vivamus fringilla est"</h5>
                                        <ul class="icon-group booking-item-rating-stars">
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                        </ul>
                                        <p>Imperdiet magna litora lorem porta condimentum leo cras aliquet turpis mattis condimentum id malesuada himenaeos praesent dictumst vitae elementum sit lacus ultrices sollicitudin quisque natoque laoreet integer id nibh<span class="booking-item-review-more"> Consectetur nullam ad vel ante pulvinar metus nullam sociis litora eleifend augue nam accumsan a etiam sit condimentum curae platea praesent venenatis nam facilisi sociosqu facilisis morbi morbi etiam fusce venenatis lacus nullam scelerisque cum inceptos viverra tortor vehicula</span>
                                        </p>
                                        <div class="booking-item-review-more-content">
                                            <p>Lacus ut habitasse posuere volutpat netus imperdiet est ante metus potenti vitae auctor sociosqu mus pharetra aliquam convallis nullam posuere inceptos magna curabitur ultrices cras mattis sodales ornare praesent euismod ultricies purus congue justo consequat</p>
                                            <p>Morbi risus volutpat odio turpis porta nisi dis cursus fringilla fringilla adipiscing cursus parturient scelerisque sit elementum conubia augue sociosqu elit pretium ridiculus rhoncus mi ullamcorper vitae sociis ultrices penatibus et quis ridiculus lectus rhoncus quis urna duis amet mus</p>
                                            <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <ul class="list booking-item-raiting-summary-list">
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Sleep</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o text-gray"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Location</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Service</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <ul class="list booking-item-raiting-summary-list">
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Clearness</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Rooms</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o text-gray"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                        </div>
                                        <p class="booking-item-review-rate">Was this review helpful?
                                            <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 17</b>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="booking-item-review-person">
                                        <a class="booking-item-review-person-avatar round" href="#">
                                            <img src="img/70x70.png" alt="Image Alternative text" title="Gamer Chick" />
                                        </a>
                                        <p class="booking-item-review-person-name"><a href="#">Ava McDonald</a>
                                        </p>
                                        <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">124 Reviews</a></small>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="booking-item-review-content">
                                        <h5>"Primis litora donec hendrerit adipiscing"</h5>
                                        <ul class="icon-group booking-item-rating-stars">
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                        </ul>
                                        <p>Cubilia sociosqu tristique cum pretium amet inceptos vel dictumst interdum nam risus viverra aenean nostra lobortis class malesuada et himenaeos vivamus ullamcorper quis fermentum nostra taciti sagittis per lobortis libero neque eros massa<span class="booking-item-review-more"> Erat suspendisse laoreet dolor id sociis aliquam mauris justo eleifend interdum ultricies litora est inceptos inceptos condimentum penatibus gravida in lectus nunc class tincidunt at tortor viverra habitasse auctor pharetra tempor quam ante placerat class mus commodo cum magna primis sed risus interdum purus et scelerisque porta natoque blandit ultricies platea phasellus gravida id eleifend aliquet odio tempus pharetra congue at orci suspendisse taciti</span>
                                        </p>
                                        <div class="booking-item-review-more-content">
                                            <p>Duis imperdiet conubia risus dictumst fringilla fermentum sem nascetur dis</p>
                                            <p>Sagittis lectus libero dolor himenaeos nullam lectus at interdum sodales nulla ac quam arcu netus pulvinar purus pulvinar laoreet maecenas egestas suspendisse quis venenatis mollis tempus ornare in mi ridiculus porttitor magnis mollis accumsan accumsan maecenas justo mus dapibus bibendum massa tempus et malesuada ornare velit turpis lorem dictumst elit dictum dolor velit</p>
                                            <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <ul class="list booking-item-raiting-summary-list">
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Sleep</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o text-gray"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Location</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Service</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <ul class="list booking-item-raiting-summary-list">
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Clearness</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div class="booking-item-raiting-list-title">Rooms</div>
                                                            <ul class="icon-group booking-item-rating-stars">
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                                <li><i class="fa fa-smile-o"></i>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                        </div>
                                        <p class="booking-item-review-rate">Was this review helpful?
                                            <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 6</b>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div class="row wrap">
                        <div class="col-md-5">
                            <p><small>1418 reviews on this activity. &nbsp;&nbsp;Showing 1 to 7</small>
                            </p>
                        </div>
                        <div class="col-md-7">
                            <ul class="pagination">
                                <li class="active"><a href="#">1</a>
                                </li>
                                <li><a href="#">2</a>
                                </li>
                                <li><a href="#">3</a>
                                </li>
                                <li><a href="#">4</a>
                                </li>
                                <li><a href="#">5</a>
                                </li>
                                <li><a href="#">6</a>
                                </li>
                                <li><a href="#">7</a>
                                </li>
                                <li class="dots">...</li>
                                <li><a href="#">43</a>
                                </li>
                                <li class="next"><a href="#">Next Page</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="gap gap-small"></div>
                    <div class="box bg-gray">
                        <h3>Write a Review</h3>
                        <form>
                            <div class="form-group">
                                <label>Review Title</label>
                                <input class="form-control" type="text" />
                            </div>
                            <div class="form-group">
                                <label>Review Text</label>
                                <textarea class="form-control" rows="6"></textarea>
                            </div>
                            <input class="btn btn-primary" type="submit" value="Leave a Review" />
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <h4>Activities Near</h4>
                    <ul class="booking-list">
                        <li>
                            <div class="booking-item booking-item-small">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="Old No7" />
                                    </div>
                                    <div class="col-xs-5">
                                        <h5 class="booking-item-title">Old No7 Bar</h5>
                                        <ul class="icon-group booking-item-rating-stars">
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-3"><span class="booking-item-price">$100</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="booking-item booking-item-small">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="AMaze" />
                                    </div>
                                    <div class="col-xs-5">
                                        <h5 class="booking-item-title">Fashion Glasses Showcase</h5>
                                        <ul class="icon-group booking-item-rating-stars">
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-3"><span class="booking-item-price">Free</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="booking-item booking-item-small">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="new york at an angle" />
                                    </div>
                                    <div class="col-xs-5">
                                        <h5 class="booking-item-title">Manhattan Skyline</h5>
                                        <ul class="icon-group booking-item-rating-stars">
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-3"><span class="booking-item-price">Free</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="booking-item booking-item-small">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="Bubbles" />
                                    </div>
                                    <div class="col-xs-5">
                                        <h5 class="booking-item-title">Music Festival</h5>
                                        <ul class="icon-group booking-item-rating-stars">
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-3"><span class="booking-item-price">$50</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="booking-item booking-item-small">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="The Big Showoff-Take 2" />
                                    </div>
                                    <div class="col-xs-5">
                                        <h5 class="booking-item-title">Extreme Biking</h5>
                                        <ul class="icon-group booking-item-rating-stars">
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star"></i>
                                            </li>
                                            <li><i class="fa fa-star-half-empty"></i>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-3"><span class="booking-item-price">$185</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="gap gap-small"></div>
        </div>



        <?php
		
		include "includes/files/footer.php";
		
		?>


        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script src="js/nicescroll.js"></script>
        <script src="js/dropit.js"></script>
        <script src="js/ionrangeslider.js"></script>
        <script src="js/icheck.js"></script>
        <script src="js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="js/typeahead.js"></script>
        <script src="js/card-payment.js"></script>
        <script src="js/magnific.js"></script>
        <script src="js/owl-carousel.js"></script>
        <script src="js/fitvids.js"></script>
        <script src="js/tweet.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/gridrotator.js"></script>
        <script src="js/custom.js"></script>
    </div>
</body>

</html>


