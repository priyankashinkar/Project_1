<!DOCTYPE HTML>
<html>

		<?php
		
		include "includes/files/header_links.php";
		
		?>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        
		 <?php
		
		include "includes/files/page_header.php";
		
		?>

        <div class="container">
            <h1 class="page-title">Travel Photos</h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <aside class="user-profile-sidebar">
                        <div class="user-profile-avatar text-center">
                            <img src="img/300x300.png" alt="Image Alternative text" title="AMaze" />
                            <h5>John Doe</h5>
                            <p>Member Since May 2012</p>
                        </div>
                        <ul class="list user-profile-nav">
                            <li><a href="user-profile.html"><i class="fa fa-user"></i>Overview</a>
                            </li>
                            <li><a href="user-profile-settings.html"><i class="fa fa-cog"></i>Settings</a>
                            </li>
                            <li><a href="user-profile-photos.html"><i class="fa fa-camera"></i>My Travel Photos</a>
                            </li>
                            <li><a href="user-profile-booking-history.html"><i class="fa fa-clock-o"></i>Booking History</a>
                            </li>
                            <li><a href="user-profile-cards.html"><i class="fa fa-credit-card"></i>Credit/Debit Cards</a>
                            </li>
                            <li><a href="user-profile-wishlist.html"><i class="fa fa-heart-o"></i>Wishlist</a>
                            </li>
                        </ul>
                    </aside>
                </div>
                <div class="col-md-9">
                    <a href="#" class="btn btn-primary mb20"><i class="fa fa-plus-circle"></i> Add new photo</a>
                    <div class="row row-no-gutter">
                        <div class="col-md-4">
                            <div class="thumb">
                                <a class="hover-img" href="#">
                                    <img src="img/400x300.png" alt="Image Alternative text" title="the journey home" />
                                    <div class="hover-inner hover-inner-block hover-inner-bottom hover-inner-bg-black hover-inner-sm hover-hold">
                                        <div class="text-small">
                                            <p><i class="fa fa-map-marker"></i> New York, NY (Midtown East)</p><small class="text-white">July 10, 2014</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="thumb">
                                <a class="hover-img" href="#">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="El inevitable paso del tiempo" />
                                    <div class="hover-inner hover-inner-block hover-inner-bottom hover-inner-bg-black hover-inner-sm hover-hold">
                                        <div class="text-small">
                                            <p><i class="fa fa-map-marker"></i> New York, NY (Midtown East)</p><small class="text-white">July 7, 2014</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="thumb">
                                <a class="hover-img" href="#">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="Viva Las Vegas" />
                                    <div class="hover-inner hover-inner-block hover-inner-bottom hover-inner-bg-black hover-inner-sm hover-hold">
                                        <div class="text-small">
                                            <p><i class="fa fa-map-marker"></i> New York, NY (Downtown - Wall Street)</p><small class="text-white">July 9, 2014</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="thumb">
                                <a class="hover-img" href="#">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="people on the beach" />
                                    <div class="hover-inner hover-inner-block hover-inner-bottom hover-inner-bg-black hover-inner-sm hover-hold">
                                        <div class="text-small">
                                            <p><i class="fa fa-map-marker"></i> Queens (LaGuardia Airport (LGA))</p><small class="text-white">July 25, 2014</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="thumb">
                                <a class="hover-img" href="#">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="Gaviota en el Top" />
                                    <div class="hover-inner hover-inner-block hover-inner-bottom hover-inner-bg-black hover-inner-sm hover-hold">
                                        <div class="text-small">
                                            <p><i class="fa fa-map-marker"></i> Flushing, NY (LaGuardia Airport (LGA))</p><small class="text-white">July 6, 2014</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="thumb">
                                <a class="hover-img" href="#">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="4 Strokes of Fun" />
                                    <div class="hover-inner hover-inner-block hover-inner-bottom hover-inner-bg-black hover-inner-sm hover-hold">
                                        <div class="text-small">
                                            <p><i class="fa fa-map-marker"></i> New York, NY (Upper West Side)</p><small class="text-white">July 8, 2014</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="thumb">
                                <a class="hover-img" href="#">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="Pictures at the museum" />
                                    <div class="hover-inner hover-inner-block hover-inner-bottom hover-inner-bg-black hover-inner-sm hover-hold">
                                        <div class="text-small">
                                            <p><i class="fa fa-map-marker"></i> Long Island City, NY (Long Island City - Astoria)</p><small class="text-white">July 3, 2014</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="thumb">
                                <a class="hover-img" href="#">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="new york at an angle" />
                                    <div class="hover-inner hover-inner-block hover-inner-bottom hover-inner-bg-black hover-inner-sm hover-hold">
                                        <div class="text-small">
                                            <p><i class="fa fa-map-marker"></i> Long Island City, NY (Long Island City - Astoria)</p><small class="text-white">July 16, 2014</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="thumb">
                                <a class="hover-img" href="#">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="Bridge" />
                                    <div class="hover-inner hover-inner-block hover-inner-bottom hover-inner-bg-black hover-inner-sm hover-hold">
                                        <div class="text-small">
                                            <p><i class="fa fa-map-marker"></i> New York, NY (Upper West Side)</p><small class="text-white">July 1, 2014</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="thumb">
                                <a class="hover-img" href="#">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="Street" />
                                    <div class="hover-inner hover-inner-block hover-inner-bottom hover-inner-bg-black hover-inner-sm hover-hold">
                                        <div class="text-small">
                                            <p><i class="fa fa-map-marker"></i> East Elmhurst, NY (LaGuardia Airport (LGA))</p><small class="text-white">July 30, 2014</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="thumb">
                                <a class="hover-img" href="#">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="The Hidden Power of the Heart" />
                                    <div class="hover-inner hover-inner-block hover-inner-bottom hover-inner-bg-black hover-inner-sm hover-hold">
                                        <div class="text-small">
                                            <p><i class="fa fa-map-marker"></i> New York, NY (Upper West Side)</p><small class="text-white">July 16, 2014</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="thumb">
                                <a class="hover-img" href="#">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="Plunklock live in Cologne" />
                                    <div class="hover-inner hover-inner-block hover-inner-bottom hover-inner-bg-black hover-inner-sm hover-hold">
                                        <div class="text-small">
                                            <p><i class="fa fa-map-marker"></i> Jamaica, NY (Kennedy Airport (JFK))</p><small class="text-white">July 21, 2014</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="thumb">
                                <a class="hover-img" href="#">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="Fly away" />
                                    <div class="hover-inner hover-inner-block hover-inner-bottom hover-inner-bg-black hover-inner-sm hover-hold">
                                        <div class="text-small">
                                            <p><i class="fa fa-map-marker"></i> Brooklyn, NY (Brooklyn)</p><small class="text-white">July 20, 2014</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="thumb">
                                <a class="hover-img" href="#">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="Pizza Hut" />
                                    <div class="hover-inner hover-inner-block hover-inner-bottom hover-inner-bg-black hover-inner-sm hover-hold">
                                        <div class="text-small">
                                            <p><i class="fa fa-map-marker"></i> Brooklyn, NY (Brooklyn)</p><small class="text-white">July 13, 2014</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="thumb">
                                <a class="hover-img" href="#">
                                    <img src="img/800x600.png" alt="Image Alternative text" title="me" />
                                    <div class="hover-inner hover-inner-block hover-inner-bottom hover-inner-bg-black hover-inner-sm hover-hold">
                                        <div class="text-small">
                                            <p><i class="fa fa-map-marker"></i> New York, NY (Times Square)</p><small class="text-white">July 2, 2014</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="gap gap-small"></div>
                    <ul class="pagination">
                        <li class="active"><a href="#">1</a>
                        </li>
                        <li><a href="#">2</a>
                        </li>
                        <li><a href="#">3</a>
                        </li>
                        <li><a href="#">4</a>
                        </li>
                        <li><a href="#">5</a>
                        </li>
                        <li><a href="#">6</a>
                        </li>
                        <li><a href="#">7</a>
                        </li>
                        <li class="dots">...</li>
                        <li><a href="#">43</a>
                        </li>
                        <li class="next"><a href="#">Next Page</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>



        <div class="gap"></div>
        
		<?php
		
		include "includes/files/footer.php";
		
		?>


        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script src="js/nicescroll.js"></script>
        <script src="js/dropit.js"></script>
        <script src="js/ionrangeslider.js"></script>
        <script src="js/icheck.js"></script>
        <script src="js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="js/typeahead.js"></script>
        <script src="js/card-payment.js"></script>
        <script src="js/magnific.js"></script>
        <script src="js/owl-carousel.js"></script>
        <script src="js/fitvids.js"></script>
        <script src="js/tweet.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/gridrotator.js"></script>
        <script src="js/custom.js"></script>
    </div>
</body>

</html>


