<!DOCTYPE HTML>
<html class="full">

<?php
		
		include "includes/files/header_links.php";
		
		?>

<body class="full">

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">

        <div class="full-page text-center">
            <div class="bg-holder full">
                <div class="bg-mask-darken"></div>
                <div class="bg-img" style="background-image:url(img/2048x2048.png);"></div>
                <div class="bg-holder-content full text-white">
                    <a class="logo-holder" href="index.html">
                        <img src="img/logo-white.png" alt="Image Alternative text" title="Image Title" />
                    </a>
                    <div class="full-center">
                        <div class="container">
                            <h2>We're Comming Soon</h2>
                            <div class="countdown countdown-lg" inline_comment="countdown" data-countdown="Jul 22, 2015" id="countdown"></div>
                            <div class="gap"></div>
                            <p>be notified. we just need your email address</p>
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <form>
                                        <div class="form-group form-group-ghost form-group-lg">
                                            <input class="form-control" placeholder="Type your email address" type="text" />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="list footer-social">
                        <li>
                            <a class="fa fa-facebook round box-icon-normal animate-icon-bounce" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-twitter round box-icon-normal animate-icon-bounce" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-dribbble round box-icon-normal animate-icon-bounce" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-linkedin round box-icon-normal animate-icon-bounce" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-pinterest round box-icon-normal animate-icon-bounce" href="#"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>



        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script src="js/nicescroll.js"></script>
        <script src="js/dropit.js"></script>
        <script src="js/ionrangeslider.js"></script>
        <script src="js/icheck.js"></script>
        <script src="js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="js/typeahead.js"></script>
        <script src="js/card-payment.js"></script>
        <script src="js/magnific.js"></script>
        <script src="js/owl-carousel.js"></script>
        <script src="js/fitvids.js"></script>
        <script src="js/tweet.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/gridrotator.js"></script>
        <script src="js/custom.js"></script>
    </div>
</body>

</html>


