<!DOCTYPE HTML>
<html>

<?php
		
		include "includes/files/header_links.php";
		
		?>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        
		 <?php
		
		include "includes/files/page_header.php";
		
		?>

        <div class="container">
            <h1 class="page-title">Traveler Blog</h1>
        </div>




        <div class="container">
            <article class="post">
                <header class="post-header">
                    <div class="fotorama" data-allowfullscreen="true">
                        <img src="img/1200x500.png" alt="Image Alternative text" title="196_365" />
                        <img src="img/1200x500.png" alt="Image Alternative text" title="196_365" />
                        <img src="img/1200x500.png" alt="Image Alternative text" title="196_365" />
                    </div>
                </header>
                <div class="post-inner">
                    <h4 class="post-title text-darken">Dignissim nam aliquam congue mauris</h4>
                    <ul class="post-meta">
                        <li><i class="fa fa-calendar"></i><a href="#">18 October, 2014</a>
                        </li>
                        <li><i class="fa fa-user"></i><a href="#">Bernadette Cornish</a>
                        </li>
                        <li><i class="fa fa-tags"></i><a href="#">Lifestyle</a>, <a href="#">Design</a>
                        </li>
                        <li><i class="fa fa-comments"></i><a href="#">10 Comments</a>
                        </li>
                    </ul>
                    <p>Lobortis quis sociosqu lorem cras pretium quam parturient euismod et fermentum curabitur cubilia elit mattis nascetur ipsum vulputate sapien viverra fermentum habitasse cras ac nibh phasellus accumsan praesent laoreet scelerisque</p>
                    <p>Id potenti nec auctor praesent class eget fringilla volutpat leo consectetur urna per tellus libero porttitor praesent vel velit vulputate nostra sapien suscipit pellentesque felis viverra suscipit adipiscing facilisis nostra dictumst vehicula mi euismod nam quisque lorem blandit dis fermentum congue nostra semper venenatis tortor velit odio molestie sed potenti id iaculis fames vivamus erat fringilla himenaeos fusce vulputate purus litora torquent sem litora morbi eleifend justo sit congue ligula aliquet placerat commodo curae in himenaeos vulputate lacinia porta pulvinar rhoncus sollicitudin pharetra conubia tempor molestie mollis id facilisis at accumsan sodales malesuada montes faucibus hac leo nam curabitur est</p>
                    <h5>Nostra dolor quis justo vestibulum auctor</h5>
                    <p>Sodales curae vitae eros odio fermentum nisi hac sodales erat fermentum lobortis porta sed condimentum diam tortor felis ipsum natoque phasellus nullam duis erat quisque bibendum vulputate fermentum habitant molestie posuere hendrerit diam felis senectus lorem leo etiam faucibus elit torquent quis orci ridiculus pretium nisl risus parturient penatibus curae dolor vitae urna est tellus fringilla elementum et enim senectus semper adipiscing fermentum aliquam iaculis sollicitudin morbi at convallis non tortor massa sem odio sociis ligula nunc risus fringilla massa fames id taciti quam aptent euismod in risus nisi sodales imperdiet platea curae tempus semper augue libero quisque cum aenean</p>
                    <h5>Porta faucibus habitasse</h5>
                    <p>Cubilia non tincidunt tristique mauris interdum fringilla quisque proin senectus vivamus integer tortor aptent phasellus sagittis nisl sociis rutrum in ut iaculis consequat dictum magna integer amet nullam hendrerit convallis in platea curabitur etiam mauris sollicitudin vehicula at erat at fusce donec vel semper lacinia urna libero arcu scelerisque varius</p>
                    <p>Commodo et mus lobortis proin justo eu eget porttitor vitae bibendum lectus sollicitudin tortor mollis sit mauris per quis nibh mus elit fermentum lorem facilisis condimentum feugiat pharetra nibh donec iaculis ultricies sagittis aenean morbi lectus praesent mollis libero ut proin dolor dapibus nunc phasellus viverra facilisis elit tortor hac</p>
                    <h5>Ut a eget facilisis pharetra nostra ad est nisl facilisis</h5>
                    <p>Consectetur eget fermentum rutrum suscipit penatibus ultrices eu bibendum mi volutpat mattis cum facilisis nunc platea tincidunt vehicula laoreet montes parturient urna magnis eu etiam eget integer nullam consectetur fames erat scelerisque ac conubia orci mauris facilisi dapibus penatibus a senectus inceptos turpis venenatis urna etiam lorem nullam lobortis risus vehicula potenti risus iaculis lacus laoreet porttitor aliquam massa mauris netus duis eu diam et tristique gravida conubia congue morbi rutrum nibh urna cras dolor euismod blandit conubia placerat lectus lacinia magna sociis quis eu porta proin lobortis malesuada potenti volutpat eu laoreet ac placerat lacinia ultrices morbi ipsum non facilisis sed etiam arcu justo quam tellus magna nullam suscipit et conubia quis augue class phasellus curae tincidunt justo himenaeos phasellus eros sodales ultrices pellentesque per ligula morbi ut auctor taciti vestibulum taciti varius cum integer eu elit turpis natoque molestie libero elementum placerat litora molestie primis lorem ipsum velit lorem curabitur eget platea auctor laoreet enim donec non montes fringilla lobortis placerat dolor sed vehicula inceptos nulla mauris non feugiat metus semper urna nec conubia montes tellus pulvinar cum aliquet tristique turpis sed curabitur vulputate pharetra sit egestas nulla cras justo commodo pulvinar tincidunt faucibus consequat sit ad potenti</p>
                    <p>Convallis suscipit consequat habitant lorem etiam euismod laoreet a bibendum conubia platea amet pretium praesent fusce libero diam pellentesque blandit natoque turpis aliquet dapibus elementum tincidunt ligula facilisi sem neque</p>
                </div>
            </article>
            <h2>Post Discussion</h2>
            <!-- START COMMENTS -->
            <ul class="comments-list">
                <li>
                    <div class="article comment" inline_comment="comment">
                        <div class="comment-author">
                            <img src="img/50x50.png" alt="Image Alternative text" title="Gamer Chick" />
                        </div>
                        <div class="comment-inner"><span class="comment-author-name">Brandon Burgess</span>
                            <p class="comment-content">Proin quam gravida proin potenti primis morbi parturient ultricies torquent viverra aptent pretium posuere proin curabitur vehicula curabitur inceptos phasellus vehicula placerat at sociosqu nam</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 15</a>
                        </div>
                    </div>
                    <ul>
                        <li>
                            <div class="article comment" inline_comment="comment">
                                <div class="comment-author">
                                    <img src="img/50x50.png" alt="Image Alternative text" title="4 Strokes of Fun" />
                                </div>
                                <div class="comment-inner"><span class="comment-author-name">Joe Smith</span>
                                    <p class="comment-content">Malesuada tellus himenaeos suspendisse ligula faucibus nulla luctus platea dis dolor ornare dapibus consectetur sit praesent hendrerit</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 18</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <li>
                        <div class="article comment" inline_comment="comment">
                            <div class="comment-author">
                                <img src="img/50x50.png" alt="Image Alternative text" title="Spidy" />
                            </div>
                            <div class="comment-inner"><span class="comment-author-name">Sarah Slater</span>
                                <p class="comment-content">At placerat rutrum vulputate mollis mus tincidunt elementum maecenas curae sit imperdiet a potenti lectus interdum phasellus varius bibendum rhoncus nostra aptent neque lectus</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 26</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="article comment" inline_comment="comment">
                            <div class="comment-author">
                                <img src="img/50x50.png" alt="Image Alternative text" title="Good job" />
                            </div>
                            <div class="comment-inner"><span class="comment-author-name">Neil Davidson</span>
                                <p class="comment-content">Montes donec ac elementum malesuada penatibus rhoncus sed cum porta feugiat suspendisse blandit adipiscing aliquet tincidunt integer taciti proin sollicitudin platea varius rhoncus dui aptent litora</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 15</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="article comment" inline_comment="comment">
                            <div class="comment-author">
                                <img src="img/50x50.png" alt="Image Alternative text" title="Ana 29" />
                            </div>
                            <div class="comment-inner"><span class="comment-author-name">Olivia Slater</span>
                                <p class="comment-content">Fames ut orci cum faucibus molestie fusce augue netus pretium molestie cum viverra lorem sociis auctor sociosqu erat feugiat auctor volutpat aliquam nisl taciti sapien etiam rhoncus pharetra blandit velit</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 3</a>
                            </div>
                        </div>
                        <ul>
                            <li>
                                <div class="article comment" inline_comment="comment">
                                    <div class="comment-author">
                                        <img src="img/50x50.png" alt="Image Alternative text" title="Luca" />
                                    </div>
                                    <div class="comment-inner"><span class="comment-author-name">Minnie Aviles</span>
                                        <p class="comment-content">Accumsan torquent fermentum fermentum senectus dapibus magnis accumsan vehicula cum commodo fringilla viverra montes</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 10</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <li>
                            <div class="article comment" inline_comment="comment">
                                <div class="comment-author">
                                    <img src="img/50x50.png" alt="Image Alternative text" title="Afro" />
                                </div>
                                <div class="comment-inner"><span class="comment-author-name">Oliver Ross</span>
                                    <p class="comment-content">Mus mollis himenaeos class lorem nam sociis congue quis imperdiet ornare elementum quam magnis turpis platea facilisis sollicitudin dolor dignissim pulvinar ultrices nullam vel ultricies</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 1</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="article comment" inline_comment="comment">
                                <div class="comment-author">
                                    <img src="img/50x50.png" alt="Image Alternative text" title="Chiara" />
                                </div>
                                <div class="comment-inner"><span class="comment-author-name">Leah Kerr</span>
                                    <p class="comment-content">Mus vulputate scelerisque eu per fermentum etiam nisi platea dui posuere nibh netus eros sed nisl senectus natoque sapien maecenas himenaeos proin in natoque porta sociis</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 35</a>
                                </div>
                            </div>
                            <ul></ul>
            </ul>
            <!-- END COMMENTS -->
            <h3>Leave a Comment</h3>
            <form>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control" type="text" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>E-mail</label>
                            <input class="form-control" type="text" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Website</label>
                            <input class="form-control" type="text" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Comment</label>
                    <textarea class="form-control"></textarea>
                </div>
                <input class="btn btn-primary" type="submit" value="Leave a Comment" />
            </form>
        </div>



        <div class="gap"></div>
      
	  <?php
		
		include "includes/files/footer.php";
		
		?>


        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script src="js/nicescroll.js"></script>
        <script src="js/dropit.js"></script>
        <script src="js/ionrangeslider.js"></script>
        <script src="js/icheck.js"></script>
        <script src="js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="js/typeahead.js"></script>
        <script src="js/card-payment.js"></script>
        <script src="js/magnific.js"></script>
        <script src="js/owl-carousel.js"></script>
        <script src="js/fitvids.js"></script>
        <script src="js/tweet.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/gridrotator.js"></script>
        <script src="js/custom.js"></script>
    </div>
</body>

</html>


