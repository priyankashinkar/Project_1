<!DOCTYPE HTML>
<html>

<?php
		
		include "includes/files/header_links.php";
		
		?>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        
		 <?php
		
		include "includes/files/page_header.php";
		
		?>

        <div class="container">
            <h1 class="page-title">Bootstrap Grid</h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <aside class="sidebar-left">
                        <ul class="nav nav-pills nav-stacked nav-side mb30">

                            <li><a href="feature-typography.html">Typography</a>
                            </li>
                            <li><a href="feature-icons.html">Icons</a>
                            </li>
                            <li><a href="feature-forms.html">Forms</a>
                            </li>
                            <li><a href="feature-icon-effects.html">Icon Effects</a>
                            </li>
                            <li><a href="feature-elements.html">Elements</a>
                            </li>
                            <li class="active"><a href="feature-grid.html">Grid</a>
                            </li>
                            <li><a href="feature-hovers.html">Hover effects</a>
                            </li>
                            <li><a href="feature-lightbox.html">Lightbox</a>
                            </li>
                            <li><a href="feature-media.html">Media</a>
                            </li>
                        </ul>
                    </aside>
                </div>
                <div class="col-md-9">
                    <div class="demo-grid">
                        <h5>1 Column</h5>
                        <div class="row">
                            <div class="col-md-1">
                                <div></div>
                            </div>
                        </div>
                        <h5>2 Columns</h5>
                        <div class="row">
                            <div class="col-md-2">
                                <div></div>
                            </div>
                        </div>
                        <h5>3 Columns</h5>
                        <div class="row">
                            <div class="col-md-3">
                                <div></div>
                            </div>
                        </div>
                        <h5>4 Columns</h5>
                        <div class="row">
                            <div class="col-md-4">
                                <div></div>
                            </div>
                        </div>
                        <h5>5 Columns</h5>
                        <div class="row">
                            <div class="col-md-5">
                                <div></div>
                            </div>
                        </div>
                        <h5>6 Columns</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <div></div>
                            </div>
                        </div>
                        <h5>7 Columns</h5>
                        <div class="row">
                            <div class="col-md-7">
                                <div></div>
                            </div>
                        </div>
                        <h5>8 Columns</h5>
                        <div class="row">
                            <div class="col-md-8">
                                <div></div>
                            </div>
                        </div>
                        <h5>9 Columns</h5>
                        <div class="row">
                            <div class="col-md-9">
                                <div></div>
                            </div>
                        </div>
                        <h5>10 Columns</h5>
                        <div class="row">
                            <div class="col-md-10">
                                <div></div>
                            </div>
                        </div>
                        <h5>11 Columns</h5>
                        <div class="row">
                            <div class="col-md-11">
                                <div></div>
                            </div>
                        </div>
                        <h5>12 Columns</h5>
                        <div class="row">
                            <div class="col-md-12">
                                <div></div>
                            </div>
                        </div>

                        <h3>Stacked</h3>
                        <h5>6 Columns</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <div></div>
                            </div>
                            <div class="col-md-6">
                                <div></div>
                            </div>
                        </div>
                        <h5>4 Columns</h5>
                        <div class="row">
                            <div class="col-md-4">
                                <div></div>
                            </div>
                            <div class="col-md-4">
                                <div></div>
                            </div>
                            <div class="col-md-4">
                                <div></div>
                            </div>
                        </div>
                        <h5>3 Columns</h5>
                        <div class="row">
                            <div class="col-md-3">
                                <div></div>
                            </div>
                            <div class="col-md-3">
                                <div></div>
                            </div>
                            <div class="col-md-3">
                                <div></div>
                            </div>
                            <div class="col-md-3">
                                <div></div>
                            </div>
                        </div>
                        <h5>2 Columns</h5>
                        <div class="row">
                            <div class="col-md-2">
                                <div></div>
                            </div>
                            <div class="col-md-2">
                                <div></div>
                            </div>
                            <div class="col-md-2">
                                <div></div>
                            </div>
                            <div class="col-md-2">
                                <div></div>
                            </div>
                            <div class="col-md-2">
                                <div></div>
                            </div>
                            <div class="col-md-2">
                                <div></div>
                            </div>
                        </div>
                        <h5>1 Column</h5>
                        <div class="row">
                            <div class="col-md-1">
                                <div></div>
                            </div>
                            <div class="col-md-1">
                                <div></div>
                            </div>
                            <div class="col-md-1">
                                <div></div>
                            </div>
                            <div class="col-md-1">
                                <div></div>
                            </div>
                            <div class="col-md-1">
                                <div></div>
                            </div>
                            <div class="col-md-1">
                                <div></div>
                            </div>
                            <div class="col-md-1">
                                <div></div>
                            </div>
                            <div class="col-md-1">
                                <div></div>
                            </div>
                            <div class="col-md-1">
                                <div></div>
                            </div>
                            <div class="col-md-1">
                                <div></div>
                            </div>
                            <div class="col-md-1">
                                <div></div>
                            </div>
                            <div class="col-md-1">
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="gap"></div>
        
		<?php
		
		include "includes/files/footer.php";
		
		?>


        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script src="js/nicescroll.js"></script>
        <script src="js/dropit.js"></script>
        <script src="js/ionrangeslider.js"></script>
        <script src="js/icheck.js"></script>
        <script src="js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="js/typeahead.js"></script>
        <script src="js/card-payment.js"></script>
        <script src="js/magnific.js"></script>
        <script src="js/owl-carousel.js"></script>
        <script src="js/fitvids.js"></script>
        <script src="js/tweet.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/gridrotator.js"></script>
        <script src="js/custom.js"></script>
    </div>
</body>

</html>


