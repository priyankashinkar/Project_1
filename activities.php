<!DOCTYPE HTML>
<html>

<?php
		
		include "includes/files/header_links.php";
		
		?>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        
		 <?php
		
		include "includes/files/page_header.php";
		
		?>





        <div class="container">
            <ul class="breadcrumb">
                <li><a href="index.html">Home</a>
                </li>
                <li><a href="#">United States</a>
                </li>
                <li><a href="#">New York (NY)</a>
                </li>
                <li><a href="#">New York City</a>
                </li>
                <li class="active">New York City Things to Do</li>
            </ul>
            <div class="mfp-with-anim mfp-hide mfp-dialog mfp-search-dialog" id="search-dialog">
                <h3>Search for Activity</h3>
                <form>
                    <div class="input-daterange" data-date-format="MM d, D">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon input-icon-highlight"></i>
                                    <label>Loation</label>
                                    <input class="typeahead form-control" placeholder="City, Country or U.S. Zip Code" type="text" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                    <label>From</label>
                                    <input class="form-control" name="start" type="text" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                    <label>To</label>
                                    <input class="form-control" name="end" type="text" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-lg" type="submit">Search for Flights</button>
                </form>
            </div>
            <h3 class="booking-title">530 things to do in New York on Mar 22 - Apr 17 <small><a class="popup-text" href="#search-dialog" data-effect="mfp-zoom-out">Change search</a></small></h3>
            <div class="row">
                <div class="col-md-3">
                    <aside class="booking-filters text-white">
                        <h3>Filter By:</h3>
                        <ul class="list booking-filters-list">
                            <li>
                                <h5 class="booking-filters-title">Star Rating</h5>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />5 star (220)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />4 star (112)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />3 star (75)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />2 star (60)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />1 star (20)</label>
                                </div>
                            </li>
                            <li>
                                <h5 class="booking-filters-title">Attractions</h5>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Perfomances (126)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Cultural (80)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Museums (130)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Landmarks (52)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Outdoors (62)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Amusement (22)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Sports (32)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Zoos & Aquariums (7)</label>
                                </div>
                            </li>
                            <li>
                                <h5 class="booking-filters-title">Activities</h5>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Sightseeing Tours (184)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Wellness & Spas (130)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Food & Drink (40)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Adventure (15)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Classes (34)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Gear Rentals (10)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Ranch & Farm (1)</label>
                                </div>
                            </li>
                            <li>
                                <h5 class="booking-filters-title">Nightlife</h5>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Bars (115)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Clubs (63)</label>
                                </div>
                            </li>
                        </ul>
                    </aside>
                </div>
                <div class="col-md-9">
                    <div class="nav-drop booking-sort">
                        <h5 class="booking-sort-title"><a href="#">Sort: Ranking<i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i></a></h5>
                        <ul class="nav-drop-menu">
                            <li><a href="#">Name (A-Z)</a>
                            </li>
                            <li><a href="#">Name (Z-A)</a>
                            </li>
                            <li><a href="#">Number of Reviews</a>
                            </li>
                            <li><a href="#">Just Added</a>
                            </li>
                        </ul>
                    </div>
                    <ul class="booking-list">
                        <li>
                            <a class="booking-item" href="#">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="Street Yoga" />
                                    </div>
                                    <div class="col-md-5">
                                        <div class="booking-item-rating">
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-o"></i>
                                                </li>
                                            </ul><span class="booking-item-rating-number"><b >3.9</b> of 5</span><small>(668 reviews)</small>
                                        </div>
                                        <h5 class="booking-item-title">Street Yoga</h5>
                                        <p class="booking-item-address"><i class="fa fa-map-marker"></i> New York, NY (Times Square)</p>
                                        <p class="booking-item-description">Platea tempor pulvinar sodales blandit orci montes posuere hac natoque</p>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$115</span><span>/person</span><span class="btn btn-primary">Add to Trip</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="#">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="4 Strokes of Fun" />
                                    </div>
                                    <div class="col-md-5">
                                        <div class="booking-item-rating">
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-o"></i>
                                                </li>
                                            </ul><span class="booking-item-rating-number"><b >3.6</b> of 5</span><small>(982 reviews)</small>
                                        </div>
                                        <h5 class="booking-item-title">Adrenaline Madness</h5>
                                        <p class="booking-item-address"><i class="fa fa-map-marker"></i> New York, NY (Chelsea)</p>
                                        <p class="booking-item-description">Feugiat amet etiam magnis pulvinar posuere lacinia nisi in nam sapien dignissim nullam at luctus luctus euismod dis</p>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$105</span><span>/person</span><span class="btn btn-primary">Add to Trip</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="#">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="AMaze" />
                                    </div>
                                    <div class="col-md-5">
                                        <div class="booking-item-rating">
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-half-empty"></i>
                                                </li>
                                            </ul><span class="booking-item-rating-number"><b >4.1</b> of 5</span><small>(818 reviews)</small>
                                        </div>
                                        <h5 class="booking-item-title">Fashion Glasses Showcase</h5>
                                        <p class="booking-item-address"><i class="fa fa-map-marker"></i> New York, NY (Times Square)</p>
                                        <p class="booking-item-description">Mi nibh semper vitae condimentum mus blandit montes dignissim malesuada facilisis bibendum</p>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">Free</span><span class="btn btn-primary">Add to Trip</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="#">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="Food is Pride" />
                                    </div>
                                    <div class="col-md-5">
                                        <div class="booking-item-rating">
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-half-empty"></i>
                                                </li>
                                            </ul><span class="booking-item-rating-number"><b >4.5</b> of 5</span><small>(1322 reviews)</small>
                                        </div>
                                        <h5 class="booking-item-title">Food is Prime</h5>
                                        <p class="booking-item-address"><i class="fa fa-map-marker"></i> New York, NY (Times Square)</p>
                                        <p class="booking-item-description">Natoque iaculis himenaeos ac nunc facilisi dignissim ridiculus elementum litora ante netus purus habitant aliquet penatibus volutpat ultrices quam praesent</p>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$100</span><span>/person</span><span class="btn btn-primary">Add to Trip</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="#">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="The Big Showoff-Take 2" />
                                    </div>
                                    <div class="col-md-5">
                                        <div class="booking-item-rating">
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-half-empty"></i>
                                                </li>
                                            </ul><span class="booking-item-rating-number"><b >4.4</b> of 5</span><small>(654 reviews)</small>
                                        </div>
                                        <h5 class="booking-item-title">Extreme Biking</h5>
                                        <p class="booking-item-address"><i class="fa fa-map-marker"></i> Bronx (Bronx)</p>
                                        <p class="booking-item-description">Urna egestas ornare suscipit velit vestibulum tortor erat viverra magnis vehicula faucibus luctus turpis ac suscipit interdum</p>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$185</span><span>/person</span><span class="btn btn-primary">Add to Trip</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="#">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="new york at an angle" />
                                    </div>
                                    <div class="col-md-5">
                                        <div class="booking-item-rating">
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-o"></i>
                                                </li>
                                            </ul><span class="booking-item-rating-number"><b >4</b> of 5</span><small>(402 reviews)</small>
                                        </div>
                                        <h5 class="booking-item-title">Manhattan Skyline</h5>
                                        <p class="booking-item-address"><i class="fa fa-map-marker"></i> New York, NY (Times Square)</p>
                                        <p class="booking-item-description">Erat facilisis habitasse libero cursus leo magnis consequat tortor parturient id fermentum dictum enim maecenas curabitur egestas</p>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">Free</span><span class="btn btn-primary">Add to Trip</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="#">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="Pictures at the museum" />
                                    </div>
                                    <div class="col-md-5">
                                        <div class="booking-item-rating">
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-half-empty"></i>
                                                </li>
                                                <li><i class="fa fa-star-o"></i>
                                                </li>
                                            </ul><span class="booking-item-rating-number"><b >3.5</b> of 5</span><small>(386 reviews)</small>
                                        </div>
                                        <h5 class="booking-item-title">The Metropolitan Museum of Art</h5>
                                        <p class="booking-item-address"><i class="fa fa-map-marker"></i> New York, NY (Times Square)</p>
                                        <p class="booking-item-description">Varius cursus turpis egestas pellentesque arcu morbi justo turpis ornare ridiculus justo parturient mauris</p>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$35</span><span>/person</span><span class="btn btn-primary">Add to Trip</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="#">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="Plunklock live in Cologne" />
                                    </div>
                                    <div class="col-md-5">
                                        <div class="booking-item-rating">
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-half-empty"></i>
                                                </li>
                                                <li><i class="fa fa-star-o"></i>
                                                </li>
                                            </ul><span class="booking-item-rating-number"><b >3.5</b> of 5</span><small>(1456 reviews)</small>
                                        </div>
                                        <h5 class="booking-item-title">After Midnight</h5>
                                        <p class="booking-item-address"><i class="fa fa-map-marker"></i> New York, NY (Midtown East)</p>
                                        <p class="booking-item-description">Senectus magnis erat dapibus volutpat quisque vitae lectus metus sociosqu aliquet semper rhoncus dis montes</p>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$350</span><span>/person</span><span class="btn btn-primary">Add to Trip</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="#">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="Spidy" />
                                    </div>
                                    <div class="col-md-5">
                                        <div class="booking-item-rating">
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-o"></i>
                                                </li>
                                            </ul><span class="booking-item-rating-number"><b >3.8</b> of 5</span><small>(236 reviews)</small>
                                        </div>
                                        <h5 class="booking-item-title">Marvel Heros is Here!</h5>
                                        <p class="booking-item-address"><i class="fa fa-map-marker"></i> Flushing, NY (LaGuardia Airport (LGA))</p>
                                        <p class="booking-item-description">Donec facilisi imperdiet lorem ornare nec ad netus hendrerit ligula curae sodales dis torquent in velit magna hac senectus</p>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$700</span><span>/person</span><span class="btn btn-primary">Add to Trip</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="#">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="Bubbles" />
                                    </div>
                                    <div class="col-md-5">
                                        <div class="booking-item-rating">
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-o"></i>
                                                </li>
                                            </ul><span class="booking-item-rating-number"><b >3.7</b> of 5</span><small>(1126 reviews)</small>
                                        </div>
                                        <h5 class="booking-item-title">Music Festival</h5>
                                        <p class="booking-item-address"><i class="fa fa-map-marker"></i> Brooklyn, NY (Brooklyn)</p>
                                        <p class="booking-item-description">Nostra in suscipit molestie suscipit mollis curabitur tincidunt consequat adipiscing potenti</p>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$50</span><span>/person</span><span class="btn btn-primary">Add to Trip</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="#">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="Trebbiano Ristorante - japenese breakfast" />
                                    </div>
                                    <div class="col-md-5">
                                        <div class="booking-item-rating">
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-half-empty"></i>
                                                </li>
                                            </ul><span class="booking-item-rating-number"><b >4.1</b> of 5</span><small>(176 reviews)</small>
                                        </div>
                                        <h5 class="booking-item-title">Tea Ceremony</h5>
                                        <p class="booking-item-address"><i class="fa fa-map-marker"></i> New York, NY (Times Square)</p>
                                        <p class="booking-item-description">Quisque eros tempus sodales sagittis volutpat nullam fames inceptos himenaeos platea lorem laoreet iaculis penatibus aptent elit lobortis ornare</p>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$300</span><span>/person</span><span class="btn btn-primary">Add to Trip</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="#">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="Me with the Uke" />
                                    </div>
                                    <div class="col-md-5">
                                        <div class="booking-item-rating">
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-half-empty"></i>
                                                </li>
                                            </ul><span class="booking-item-rating-number"><b >4.3</b> of 5</span><small>(743 reviews)</small>
                                        </div>
                                        <h5 class="booking-item-title">Ukle Master Class</h5>
                                        <p class="booking-item-address"><i class="fa fa-map-marker"></i> Flushing, NY (LaGuardia Airport (LGA))</p>
                                        <p class="booking-item-description">Nec orci a laoreet tristique primis ultrices nunc nam duis tristique eu</p>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">Free</span><span class="btn btn-primary">Add to Trip</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="#">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="Upper Lake in New York Central Park" />
                                    </div>
                                    <div class="col-md-5">
                                        <div class="booking-item-rating">
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-half-empty"></i>
                                                </li>
                                            </ul><span class="booking-item-rating-number"><b >4.6</b> of 5</span><small>(1496 reviews)</small>
                                        </div>
                                        <h5 class="booking-item-title">Central Park Trip</h5>
                                        <p class="booking-item-address"><i class="fa fa-map-marker"></i> New York, NY (Times Square)</p>
                                        <p class="booking-item-description">Commodo tristique est tincidunt cubilia cras vel rhoncus tristique malesuada</p>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">Free</span><span class="btn btn-primary">Add to Trip</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="#">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="Old No7" />
                                    </div>
                                    <div class="col-md-5">
                                        <div class="booking-item-rating">
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-half-empty"></i>
                                                </li>
                                                <li><i class="fa fa-star-o"></i>
                                                </li>
                                            </ul><span class="booking-item-rating-number"><b >3.5</b> of 5</span><small>(858 reviews)</small>
                                        </div>
                                        <h5 class="booking-item-title">Old No7 Bar</h5>
                                        <p class="booking-item-address"><i class="fa fa-map-marker"></i> New York, NY (Midtown East)</p>
                                        <p class="booking-item-description">Nullam viverra odio dui ornare suspendisse nam est conubia quisque phasellus in tristique penatibus neque faucibus</p>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$100</span><span>/person</span><span class="btn btn-primary">Add to Trip</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="#">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="img/800x600.png" alt="Image Alternative text" title="Department of Theatre Arts" />
                                    </div>
                                    <div class="col-md-5">
                                        <div class="booking-item-rating">
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-half-empty"></i>
                                                </li>
                                            </ul><span class="booking-item-rating-number"><b >4.3</b> of 5</span><small>(1408 reviews)</small>
                                        </div>
                                        <h5 class="booking-item-title">Beautiful - The Carole King Musical</h5>
                                        <p class="booking-item-address"><i class="fa fa-map-marker"></i> Jamaica, NY (Kennedy Airport (JFK))</p>
                                        <p class="booking-item-description">Conubia leo senectus tempus ante porta vulputate dis euismod sagittis risus pretium facilisis sociis in facilisis arcu interdum montes</p>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$100</span><span>/person</span><span class="btn btn-primary">Add to Trip</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <div class="row">
                        <div class="col-md-6">
                            <p><small>530 things to do in New York. &nbsp;&nbsp;Showing 1 – 15</small>
                            </p>
                            <ul class="pagination">
                                <li class="active"><a href="#">1</a>
                                </li>
                                <li><a href="#">2</a>
                                </li>
                                <li><a href="#">3</a>
                                </li>
                                <li><a href="#">4</a>
                                </li>
                                <li><a href="#">5</a>
                                </li>
                                <li><a href="#">6</a>
                                </li>
                                <li><a href="#">7</a>
                                </li>
                                <li class="dots">...</li>
                                <li><a href="#">43</a>
                                </li>
                                <li class="next"><a href="#">Next Page</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6 text-right">
                            <p>Not what you're looking for? <a class="popup-text" href="#search-dialog" data-effect="mfp-zoom-out">Try your search again</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="gap"></div>
        </div>



        <?php
		
		include "includes/files/footer.php";
		
		?>


        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script src="js/nicescroll.js"></script>
        <script src="js/dropit.js"></script>
        <script src="js/ionrangeslider.js"></script>
        <script src="js/icheck.js"></script>
        <script src="js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="js/typeahead.js"></script>
        <script src="js/card-payment.js"></script>
        <script src="js/magnific.js"></script>
        <script src="js/owl-carousel.js"></script>
        <script src="js/fitvids.js"></script>
        <script src="js/tweet.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/gridrotator.js"></script>
        <script src="js/custom.js"></script>
    </div>
</body>

</html>


