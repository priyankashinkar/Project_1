<!DOCTYPE HTML>
<html>
<?php
		
		include "includes/files/header_links.php";
		
		?>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        
		 <?php
		
		include "includes/files/page_header.php";
		
		?>

        <div class="container">
            <h1 class="page-title">Traveler Blog</h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <article class="post">
                        <header class="post-header">
                            <div class="fotorama" data-allowfullscreen="true">
                                <img src="img/1200x500.png" alt="Image Alternative text" title="196_365" />
                                <img src="img/1200x500.png" alt="Image Alternative text" title="196_365" />
                                <img src="img/1200x500.png" alt="Image Alternative text" title="196_365" />
                            </div>
                        </header>
                        <div class="post-inner">
                            <h4 class="post-title text-darken">Ipsum dui nisl odio bibendum</h4>
                            <ul class="post-meta">
                                <li><i class="fa fa-calendar"></i><a href="#">19 October, 2014</a>
                                </li>
                                <li><i class="fa fa-user"></i><a href="#">Keith Churchill</a>
                                </li>
                                <li><i class="fa fa-tags"></i><a href="#">Web</a>
                                </li>
                                <li><i class="fa fa-comments"></i><a href="#">0 Comments</a>
                                </li>
                            </ul>
                            <p>Vivamus semper interdum consectetur nam id eu suspendisse ipsum in magna curae metus laoreet pulvinar habitant gravida non enim eu sollicitudin mattis purus facilisis mattis eleifend lectus elit facilisis hendrerit</p>
                            <p>Arcu litora pharetra a sem sagittis non proin lacinia neque nec per consectetur pharetra montes imperdiet nisi ipsum vitae viverra class orci sodales aptent fermentum posuere nostra porta massa libero erat malesuada habitasse dui elit mus class sodales mollis lobortis lectus phasellus nostra felis nostra nascetur lectus posuere fringilla suscipit luctus primis sem id posuere vitae risus magna netus molestie lectus aliquam class accumsan metus gravida placerat magna enim est gravida natoque feugiat curabitur mi gravida viverra sociosqu dui integer varius per taciti sollicitudin integer magnis leo eros condimentum parturient lectus quam enim viverra scelerisque tortor natoque sodales ac scelerisque</p>
                            <h5>Etiam at adipiscing ante class vulputate</h5>
                            <p>Felis purus eleifend luctus suspendisse luctus lacinia suspendisse consequat gravida tellus vivamus at urna fames dictumst cursus fusce varius felis imperdiet curae magna pretium dolor luctus dictumst elementum nec tristique proin duis vivamus metus hac malesuada id ultricies risus lacinia enim dui dui sodales mus vitae id class rhoncus facilisis malesuada adipiscing sollicitudin sapien euismod vitae et mollis venenatis sagittis ac cras enim semper mauris pharetra hac torquent risus quam accumsan hendrerit erat ligula lacus bibendum ultricies in fringilla id elementum orci ut condimentum dictum sed velit nisl dui vestibulum nulla potenti faucibus pulvinar odio sagittis urna dolor eleifend penatibus</p>
                            <h5>Aptent vehicula consequat</h5>
                            <p>Phasellus cubilia augue mi platea cursus dictum cursus aliquet nibh habitasse elit nam in dictumst massa nisl aliquam risus cum ad magna praesent tristique convallis duis condimentum viverra sed dolor metus condimentum himenaeos tortor auctor magnis phasellus etiam habitant taciti auctor semper lectus laoreet in luctus vehicula ultricies viverra primis</p>
                            <p>Venenatis mus platea mollis auctor amet suscipit id iaculis nisl fermentum egestas lobortis venenatis laoreet mi lectus fusce amet et torquent lectus maecenas ultrices turpis magnis nisi laoreet bibendum egestas proin facilisis mauris sociosqu pulvinar lobortis scelerisque aptent vel justo aenean condimentum duis consequat sapien porta tempus aliquam dictumst aptent</p>
                            <h5>Risus cras sed torquent est torquent purus lacus dictum et</h5>
                            <p>Nibh in taciti pulvinar libero volutpat cursus et turpis vehicula lacinia fringilla aptent luctus maecenas torquent magna fames porta justo faucibus ornare torquent tempus commodo duis lacinia purus velit elit cras viverra luctus laoreet hendrerit arcu quis adipiscing venenatis vel imperdiet potenti et consequat ligula primis convallis mollis nascetur sagittis tincidunt pharetra sed consectetur id magnis rhoncus justo velit lobortis morbi urna lobortis nascetur pulvinar hac elit erat lectus ultricies cras massa elementum habitant magna rutrum eleifend suscipit cum arcu accumsan cum libero sed pharetra ligula elementum viverra sem malesuada tellus auctor mattis magna diam aenean iaculis curabitur malesuada feugiat malesuada posuere platea ut lobortis taciti blandit fermentum imperdiet sollicitudin mollis ligula sociosqu id in semper ultrices pharetra luctus habitasse egestas libero curabitur montes sollicitudin urna erat hac imperdiet sit conubia et urna hac nostra elementum augue erat arcu proin turpis pretium faucibus proin aliquam magna posuere porta mollis rhoncus class nibh ad etiam sem tortor nascetur elementum posuere natoque ridiculus himenaeos convallis blandit pulvinar torquent ullamcorper justo aptent metus interdum adipiscing sem lorem libero tempus non lorem inceptos libero molestie proin augue posuere dictumst vivamus sollicitudin consequat parturient lacinia mollis pellentesque molestie sapien massa rutrum ultrices facilisis netus lorem</p>
                            <p>Interdum pellentesque convallis dictumst blandit commodo vitae tristique mus fringilla litora bibendum tristique nec lectus porttitor primis mus erat egestas consectetur magna purus nostra per blandit eros habitasse luctus nec</p>
                        </div>
                    </article>
                    <h2>Post Discussion</h2>
                    <!-- START COMMENTS -->
                    <ul class="comments-list">
                        <li>
                            <div class="article comment" inline_comment="comment">
                                <div class="comment-author">
                                    <img src="img/50x50.png" alt="Image Alternative text" title="Gamer Chick" />
                                </div>
                                <div class="comment-inner"><span class="comment-author-name">Brandon Burgess</span>
                                    <p class="comment-content">Iaculis dictumst dui taciti himenaeos taciti arcu non sollicitudin viverra id blandit cursus ac</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 35</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="article comment" inline_comment="comment">
                                <div class="comment-author">
                                    <img src="img/50x50.png" alt="Image Alternative text" title="4 Strokes of Fun" />
                                </div>
                                <div class="comment-inner"><span class="comment-author-name">Frank Mills</span>
                                    <p class="comment-content">Facilisis mollis vehicula vivamus consectetur ligula proin proin sociosqu sagittis facilisi nec nascetur quisque lobortis maecenas tortor class nisl ultrices</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 7</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="article comment" inline_comment="comment">
                                <div class="comment-author">
                                    <img src="img/50x50.png" alt="Image Alternative text" title="Spidy" />
                                </div>
                                <div class="comment-inner"><span class="comment-author-name">Bernadette Cornish</span>
                                    <p class="comment-content">Iaculis suspendisse libero posuere fermentum enim dictumst malesuada fames placerat</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 27</a>
                                </div>
                            </div>
                            <ul>
                                <li>
                                    <div class="article comment" inline_comment="comment">
                                        <div class="comment-author">
                                            <img src="img/50x50.png" alt="Image Alternative text" title="Good job" />
                                        </div>
                                        <div class="comment-inner"><span class="comment-author-name">Joseph Watson</span>
                                            <p class="comment-content">Dolor adipiscing quis amet id cubilia euismod primis amet porta</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 21</a>
                                        </div>
                                    </div>
                                    <ul>
                                        <li>
                                            <div class="article comment" inline_comment="comment">
                                                <div class="comment-author">
                                                    <img src="img/50x50.png" alt="Image Alternative text" title="Ana 29" />
                                                </div>
                                                <div class="comment-inner"><span class="comment-author-name">Olivia Slater</span>
                                                    <p class="comment-content">Molestie vehicula eu arcu praesent commodo sociis nunc duis vel sem senectus nunc</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 28</a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <li>
                                        <div class="article comment" inline_comment="comment">
                                            <div class="comment-author">
                                                <img src="img/50x50.png" alt="Image Alternative text" title="Luca" />
                                            </div>
                                            <div class="comment-inner"><span class="comment-author-name">Leah Kerr</span>
                                                <p class="comment-content">Eros est aliquam ad ridiculus nam ultricies suscipit penatibus duis nullam amet per netus non phasellus vulputate massa sodales habitant himenaeos</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 5</a>
                                            </div>
                                        </div>
                                    </li>
                            </ul>
                            <li>
                                <div class="article comment" inline_comment="comment">
                                    <div class="comment-author">
                                        <img src="img/50x50.png" alt="Image Alternative text" title="Afro" />
                                    </div>
                                    <div class="comment-inner"><span class="comment-author-name">Frank Mills</span>
                                        <p class="comment-content">Aenean taciti facilisis eget in justo metus suscipit elit ipsum duis donec ut faucibus maecenas mattis facilisis augue ullamcorper maecenas platea dignissim nisi habitant varius non</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 7</a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="article comment" inline_comment="comment">
                                    <div class="comment-author">
                                        <img src="img/50x50.png" alt="Image Alternative text" title="Chiara" />
                                    </div>
                                    <div class="comment-inner"><span class="comment-author-name">John Doe</span>
                                        <p class="comment-content">Mi massa himenaeos ipsum eleifend potenti tempor pretium nunc magnis dignissim molestie fermentum natoque ornare faucibus imperdiet cursus class ad lacus viverra tempor cras mollis eros sociosqu</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 20</a>
                                    </div>
                                </div>
                            </li>
                    </ul>
                    <!-- END COMMENTS -->
                    <h3>Leave a Comment</h3>
                    <form>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input class="form-control" type="text" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>E-mail</label>
                                    <input class="form-control" type="text" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Website</label>
                                    <input class="form-control" type="text" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Comment</label>
                            <textarea class="form-control"></textarea>
                        </div>
                        <input class="btn btn-primary" type="submit" value="Leave a Comment" />
                    </form>
                </div>
                <div class="col-md-3">
                    <aside class="sidebar-right">
                        <div class="sidebar-widget">
                            <div class="Form">
                                <input class="form-control" placeholder="Search..." type="text" />
                            </div>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Categories</h4>
                            <ul class="icon-list list-category">
                                <li><a href="#"><i class="fa fa-angle-right"></i>Photos <small >(94)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Vacation <small >(61)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Flights <small >(92)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Travel Advices <small >(100)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Trending Now <small >(64)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Hotels <small >(90)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Places to Go <small >(74)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Travel Stories <small >(55)</small></a>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Popular Posts</h4>
                            <ul class="thumb-list">
                                <li>
                                    <a href="#">
                                        <img src="img/70x70.png" alt="Image Alternative text" title="Viva Las Vegas" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">Jul 18, 2014</p>
                                        <h5 class="thumb-list-item-title"><a href="#">Nulla faucibus</a></h5>
                                        <p class="thumb-list-item-desciption">Porttitor fermentum vel sociosqu mollis</p>
                                    </div>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/70x70.png" alt="Image Alternative text" title="4 Strokes of Fun" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">Jul 18, 2014</p>
                                        <h5 class="thumb-list-item-title"><a href="#">Dapibus class</a></h5>
                                        <p class="thumb-list-item-desciption">Potenti luctus pretium mattis aliquam</p>
                                    </div>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/70x70.png" alt="Image Alternative text" title="Cup on red" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">Jul 18, 2014</p>
                                        <h5 class="thumb-list-item-title"><a href="#">Purus sociis</a></h5>
                                        <p class="thumb-list-item-desciption">Dictum in aptent sociosqu nascetur</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Twitter Feed</h4>
                            <div class="twitter" id="twitter"></div>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Recent Comments</h4>
                            <ul class="thumb-list thumb-list-right">
                                <li>
                                    <a href="#">
                                        <img class="rounded" src="img/70x70.png" alt="Image Alternative text" title="Afro" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">5 minutes ago</p>
                                        <h4 class="thumb-list-item-title"><a href="#">Sarah Slater</a></h4>
                                        <p class="thumb-list-item-desciption">Porttitor sodales ipsum integer porta quam vitae...</p>
                                    </div>
                                </li>
                                <li>
                                    <a href="#">
                                        <img class="rounded" src="img/70x70.png" alt="Image Alternative text" title="Gamer Chick" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">7 minutes ago</p>
                                        <h4 class="thumb-list-item-title"><a href="#">Neil Davidson</a></h4>
                                        <p class="thumb-list-item-desciption">Commodo penatibus ornare gravida porttitor vulputate dignissim...</p>
                                    </div>
                                </li>
                                <li>
                                    <a href="#">
                                        <img class="rounded" src="img/70x70.png" alt="Image Alternative text" title="AMaze" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">9 minutes ago</p>
                                        <h4 class="thumb-list-item-title"><a href="#">Blake Hardacre</a></h4>
                                        <p class="thumb-list-item-desciption">Iaculis taciti quis pellentesque netus nostra eu...</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Archive</h4>
                            <ul class="icon-list list-category">
                                <li><a href="#"><i class="fa fa-angle-right"></i>July 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>June 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>May 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>April 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>March 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>February 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>January 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>December 2014</a>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Gallery</h4>
                            <div class="row row-no-gutter">
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="Spidy" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="a dreamy jump" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="Me with the Uke" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="b and w camera" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="The Big Showoff-Take 2" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="4 Strokes of Fun" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="Happy Bokeh Day" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="Good job" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="sunny wood" />
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Facebook</h4>
                            <div class="fb-like-box" data-href="https://www.facebook.com/FacebookDevelopers" data-colorscheme="light" data-show-faces="1" data-header="1" data-show-border="1" data-width="233"></div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>



        <div class="gap"></div>
        
		<?php
		
		include "includes/files/footer.php";
		
		?>


        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script src="js/nicescroll.js"></script>
        <script src="js/dropit.js"></script>
        <script src="js/ionrangeslider.js"></script>
        <script src="js/icheck.js"></script>
        <script src="js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="js/typeahead.js"></script>
        <script src="js/card-payment.js"></script>
        <script src="js/magnific.js"></script>
        <script src="js/owl-carousel.js"></script>
        <script src="js/fitvids.js"></script>
        <script src="js/tweet.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/gridrotator.js"></script>
        <script src="js/custom.js"></script>
    </div>
</body>

</html>


