<!DOCTYPE HTML>
<html>

		<?php
		
		include "includes/files/header_links.php";
		
		?>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        
		 <?php
		
		include "includes/files/page_header.php";
		
		?>

        <div class="container">
            <h1 class="page-title">Credit/Debit Cards</h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <aside class="user-profile-sidebar">
                        <div class="user-profile-avatar text-center">
                            <img src="img/300x300.png" alt="Image Alternative text" title="AMaze" />
                            <h5>John Doe</h5>
                            <p>Member Since May 2012</p>
                        </div>
                        <ul class="list user-profile-nav">
                            <li><a href="user-profile.html"><i class="fa fa-user"></i>Overview</a>
                            </li>
                            <li><a href="user-profile-settings.html"><i class="fa fa-cog"></i>Settings</a>
                            </li>
                            <li><a href="user-profile-photos.html"><i class="fa fa-camera"></i>My Travel Photos</a>
                            </li>
                            <li><a href="user-profile-booking-history.html"><i class="fa fa-clock-o"></i>Booking History</a>
                            </li>
                            <li><a href="user-profile-cards.html"><i class="fa fa-credit-card"></i>Credit/Debit Cards</a>
                            </li>
                            <li><a href="user-profile-wishlist.html"><i class="fa fa-heart-o"></i>Wishlist</a>
                            </li>
                        </ul>
                    </aside>
                </div>
                <div class="col-md-9">
                    <div class="mfp-with-anim mfp-hide mfp-dialog" id="edit-card-dialog">
                        <h3 class="mb0">Edit Card</h3>
                        <p>Visa XXXX XXXX XXXX 1234</p>
                        <form class="cc-form">
                            <div class="clearfix">
                                <div class="form-group form-group-cc-number">
                                    <label>Card Number</label>
                                    <input class="form-control" placeholder="xxxx xxxx xxxx xxxx" type="text" /><span class="cc-card-icon"></span>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="form-group form-group-cc-name">
                                    <label>Cardholder Name</label>
                                    <input class="form-control" value="John Doe" type="text" />
                                </div>
                                <div class="form-group form-group-cc-date">
                                    <label>Valid Thru</label>
                                    <input class="form-control" placeholder="mm/yy" type="text" />
                                </div>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="i-check" type="checkbox" />Set as primary</label>
                            </div>
                            <ul class="list-inline">
                                <li>
                                    <input class="btn btn-primary" type="submit" value="Edit Card" />
                                </li>
                                <li>
                                    <button class="btn btn-primary"><i class="fa fa-times"></i> Remove Card</button>
                                </li>
                            </ul>
                        </form>
                    </div>
                    <div class="mfp-with-anim mfp-hide mfp-dialog" id="new-card-dialog">
                        <h3>New Card</h3>
                        <form class="cc-form">
                            <div class="clearfix">
                                <div class="form-group form-group-cc-number">
                                    <label>Card Number</label>
                                    <input class="form-control" placeholder="xxxx xxxx xxxx xxxx" type="text" /><span class="cc-card-icon"></span>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="form-group form-group-cc-name">
                                    <label>Cardholder Name</label>
                                    <input class="form-control" value="John Doe" type="text" />
                                </div>
                                <div class="form-group form-group-cc-date">
                                    <label>Valid Thru</label>
                                    <input class="form-control" placeholder="mm/yy" type="text" />
                                </div>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="i-check" type="checkbox" checked/>Set as primary</label>
                            </div>
                            <input class="btn btn-primary" type="submit" value="Add Card" />
                        </form>
                    </div>
                    <div class="row row-wrap">
                        <div class="col-md-4">
                            <div class="card-thumb">
                                <ul class="card-thumb-actions">
                                    <li>
                                        <a class="fa fa-pencil popup-text" href="#edit-card-dialog" rel="tooltip" data-placement="top" title="edit" data-effect="mfp-zoom-out"></a>
                                    </li>
                                    <li>
                                        <a class="fa fa-times" href="#" rel="tooltip" data-placement="top" title="remove"></a>
                                    </li>
                                </ul>
                                <p class="card-thumb-number">XXXX XXX XXXX 6058</p>
                                <p class="card-thumb-valid">valid thru <span>3 / 16</span>
                                </p>
                                <img class="card-thumb-type" src="img/payment/american-express-curved-32px.png" alt="Image Alternative text" title="Image Title" /><small>cardholder name</small>
                                <h5 class="uc">John Doe</h5>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card-thumb card-thumb-primary"><span class="card-thumb-primary-label">primary</span>
                                <ul class="card-thumb-actions">
                                    <li>
                                        <a class="fa fa-pencil popup-text" href="#edit-card-dialog" rel="tooltip" data-placement="top" title="edit" data-effect="mfp-zoom-out"></a>
                                    </li>
                                    <li>
                                        <a class="fa fa-times" href="#" rel="tooltip" data-placement="top" title="remove"></a>
                                    </li>
                                </ul>
                                <p class="card-thumb-number">XXXX XXX XXXX 3961</p>
                                <p class="card-thumb-valid">valid thru <span>9 / 18</span>
                                </p>
                                <img class="card-thumb-type" src="img/payment/american-express-curved-32px.png" alt="Image Alternative text" title="Image Title" /><small>cardholder name</small>
                                <h5 class="uc">John Doe</h5>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card-thumb">
                                <ul class="card-thumb-actions">
                                    <li>
                                        <a class="fa fa-pencil popup-text" href="#edit-card-dialog" rel="tooltip" data-placement="top" title="edit" data-effect="mfp-zoom-out"></a>
                                    </li>
                                    <li>
                                        <a class="fa fa-times" href="#" rel="tooltip" data-placement="top" title="remove"></a>
                                    </li>
                                </ul>
                                <p class="card-thumb-number">XXXX XXX XXXX 2065</p>
                                <p class="card-thumb-valid">valid thru <span>3 / 15</span>
                                </p>
                                <img class="card-thumb-type" src="img/payment/american-express-curved-32px.png" alt="Image Alternative text" title="Image Title" /><small>cardholder name</small>
                                <h5 class="uc">John Doe</h5>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card-thumb">
                                <ul class="card-thumb-actions">
                                    <li>
                                        <a class="fa fa-pencil popup-text" href="#edit-card-dialog" rel="tooltip" data-placement="top" title="edit" data-effect="mfp-zoom-out"></a>
                                    </li>
                                    <li>
                                        <a class="fa fa-times" href="#" rel="tooltip" data-placement="top" title="remove"></a>
                                    </li>
                                </ul>
                                <p class="card-thumb-number">XXXX XXX XXXX 9080</p>
                                <p class="card-thumb-valid">valid thru <span>1 / 15</span>
                                </p>
                                <img class="card-thumb-type" src="img/payment/mastercard-curved-32px.png" alt="Image Alternative text" title="Image Title" /><small>cardholder name</small>
                                <h5 class="uc">John Doe</h5>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card-thumb">
                                <ul class="card-thumb-actions">
                                    <li>
                                        <a class="fa fa-pencil popup-text" href="#edit-card-dialog" rel="tooltip" data-placement="top" title="edit" data-effect="mfp-zoom-out"></a>
                                    </li>
                                    <li>
                                        <a class="fa fa-times" href="#" rel="tooltip" data-placement="top" title="remove"></a>
                                    </li>
                                </ul>
                                <p class="card-thumb-number">XXXX XXX XXXX 3122</p>
                                <p class="card-thumb-valid">valid thru <span>3 / 16</span>
                                </p>
                                <img class="card-thumb-type" src="img/payment/mastercard-curved-32px.png" alt="Image Alternative text" title="Image Title" /><small>cardholder name</small>
                                <h5 class="uc">John Doe</h5>
                            </div>
                        </div>
                        <div class="col-md-4"><a class="card-thumb popup-text" href="#new-card-dialog" data-effect="mfp-zoom-out"><i class="fa fa-plus card-thumb-new"></i><p >add new card</p></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="gap"></div>
        
		<?php
		
		include "includes/files/footer.php";
		
		?>


        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script src="js/nicescroll.js"></script>
        <script src="js/dropit.js"></script>
        <script src="js/ionrangeslider.js"></script>
        <script src="js/icheck.js"></script>
        <script src="js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="js/typeahead.js"></script>
        <script src="js/card-payment.js"></script>
        <script src="js/magnific.js"></script>
        <script src="js/owl-carousel.js"></script>
        <script src="js/fitvids.js"></script>
        <script src="js/tweet.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/gridrotator.js"></script>
        <script src="js/custom.js"></script>
    </div>
</body>

</html>


