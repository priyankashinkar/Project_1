<!DOCTYPE HTML>
<html>

<?php
		
		include "includes/files/header_links.php";
		
		?>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        
		 <?php
		
		include "includes/files/page_header.php";
		
		?>





        <div class="container">
            <ul class="breadcrumb">
                <li><a href="index.html">Home</a>
                </li>
                <li><a href="#">United States</a>
                </li>
                <li><a href="#">New York (NY)</a>
                </li>
                <li><a href="#">New York City</a>
                </li>
                <li><a href="#">New York City Hotels</a>
                </li>
                <li class="active">InterContinental New York Barclay</li>
            </ul>
            <div class="booking-item-details">
                <header class="booking-item-header">
                    <div class="row">
                        <div class="col-md-9">
                            <h2 class="lh1em">InterContinental New York Barclay</h2>
                            <p class="lh1em text-small"><i class="fa fa-map-marker"></i> 6782 Sarasea Circle, Siesta Key, FL 34242</p>
                            <ul class="list list-inline text-small">
                                <li><a href="#"><i class="fa fa-envelope"></i> Hotel E-mail</a>
                                </li>
                                <li><a href="#"><i class="fa fa-home"></i> Hotel Website</a>
                                </li>
                                <li><i class="fa fa-phone"></i> +1 (874) 728-9812</li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <p class="booking-item-header-price"><small>price from</small>  <span class="text-lg">$350</span>/night</p>
                        </div>
                    </div>
                </header>
                <div class="row">
                    <div class="col-md-6">
                        <div class="tabbable booking-details-tabbable">
                            <ul class="nav nav-tabs" id="myTab">
                                <li class="active"><a href="#tab-1" data-toggle="tab"><i class="fa fa-camera"></i>Photos</a>
                                </li>
                                <li><a href="#google-map-tab" data-toggle="tab"><i class="fa fa-map-marker"></i>On the Map</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab-1">
                                    <!-- START LIGHTBOX GALLERY -->
                                    <div class="row row-no-gutter" id="popup-gallery">
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="The pool" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="hotel THE CLIFF BAY spa suite" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="hotel PORTO BAY RIO INTERNACIONAL de luxe" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="hotel EDEN MAR suite" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="LHOTEL PORTO BAY SAO PAULO lobby" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="hotel PORTO BAY SERRA GOLF suite" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="hotel PORTO BAY RIO INTERNACIONAL rooftop pool" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="hotel 1" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="hotel PORTO BAY LIBERDADE" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="hotel PORTO BAY SERRA GOLF suite2" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="LHOTEL PORTO BAY SAO PAULO suite lhotel living room" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="hotel PORTO BAY SERRA GOLF living room" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="hotel 2" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="hotel PORTO BAY SERRA GOLF library" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="LHOTEL PORTO BAY SAO PAULO luxury suite" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                                <img src="img/800x600.png" alt="Image Alternative text" title="LHOTEL PORTO BAY SAO PAULO suite lhotel living room" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- END LIGHTBOX GALLERY -->
                                </div>
                                <div class="tab-pane fade" id="google-map-tab">
                                    <div id="map-canvas" style="width:100%; height:500px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="booking-item-meta">
                            <h2 class="lh1em mt40">Exeptional!</h2>
                            <h3>97% <small >of guests recommend</small></h3>
                            <div class="booking-item-rating">
                                <ul class="icon-list icon-group booking-item-rating-stars">
                                    <li><i class="fa fa-star"></i>
                                    </li>
                                    <li><i class="fa fa-star"></i>
                                    </li>
                                    <li><i class="fa fa-star"></i>
                                    </li>
                                    <li><i class="fa fa-star"></i>
                                    </li>
                                    <li><i class="fa fa-star"></i>
                                    </li>
                                </ul><span class="booking-item-rating-number"><b >4.7</b> of 5 <small class="text-smaller">guest rating</small></span>
                                <p><a class="text-default" href="#">based on 1535 reviews</a>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="lh1em">Traveler raiting</h4>
                                <ul class="list booking-item-raiting-list">
                                    <li>
                                        <div class="booking-item-raiting-list-title">Exellent</div>
                                        <div class="booking-item-raiting-list-bar">
                                            <div style="width:95%;"></div>
                                        </div>
                                        <div class="booking-item-raiting-list-number">1246</div>
                                    </li>
                                    <li>
                                        <div class="booking-item-raiting-list-title">Very Good</div>
                                        <div class="booking-item-raiting-list-bar">
                                            <div style="width:7%;"></div>
                                        </div>
                                        <div class="booking-item-raiting-list-number">75</div>
                                    </li>
                                    <li>
                                        <div class="booking-item-raiting-list-title">Average</div>
                                        <div class="booking-item-raiting-list-bar">
                                            <div style="width:3%;"></div>
                                        </div>
                                        <div class="booking-item-raiting-list-number">35</div>
                                    </li>
                                    <li>
                                        <div class="booking-item-raiting-list-title">Poor</div>
                                        <div class="booking-item-raiting-list-bar">
                                            <div style="width:3%;"></div>
                                        </div>
                                        <div class="booking-item-raiting-list-number">14</div>
                                    </li>
                                    <li>
                                        <div class="booking-item-raiting-list-title">Terrible</div>
                                        <div class="booking-item-raiting-list-bar">
                                            <div style="width:1%;"></div>
                                        </div>
                                        <div class="booking-item-raiting-list-number">7</div>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <h4 class="lh1em">Summary</h4>
                                <ul class="list booking-item-raiting-summary-list">
                                    <li>
                                        <div class="booking-item-raiting-list-title">Sleep</div>
                                        <ul class="icon-group booking-item-rating-stars">
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="booking-item-raiting-list-title">Location</div>
                                        <ul class="icon-group booking-item-rating-stars">
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o text-gray"></i>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="booking-item-raiting-list-title">Service</div>
                                        <ul class="icon-group booking-item-rating-stars">
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="booking-item-raiting-list-title">Clearness</div>
                                        <ul class="icon-group booking-item-rating-stars">
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="booking-item-raiting-list-title">Rooms</div>
                                        <ul class="icon-group booking-item-rating-stars">
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                            <li><i class="fa fa-smile-o"></i>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <a href="#" class="btn btn-primary">Write a Review</a>
                    </div>
                </div>
                <div class="gap"></div>
                <h3>Available Rooms</h3>
                <div class="row">
                    <div class="col-md-9">
                        <div class="booking-item-dates-change">
                            <form>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-daterange" data-date-format="MM d, D">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-hightlight"></i>
                                                        <label>Check in</label>
                                                        <input class="form-control" name="start" type="text" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-hightlight"></i>
                                                        <label>Check out</label>
                                                        <input class="form-control" name="end" type="text" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group form-group-select-plus">
                                            <label>Adults</label>
                                            <div class="btn-group btn-group-select-num" data-toggle="buttons">
                                                <label class="btn btn-primary active">
                                                    <input type="radio" name="options" />1</label>
                                                <label class="btn btn-primary">
                                                    <input type="radio" name="options" />2</label>
                                                <label class="btn btn-primary">
                                                    <input type="radio" name="options" />3</label>
                                                <label class="btn btn-primary">
                                                    <input type="radio" name="options" />3+</label>
                                            </div>
                                            <select class="form-control hidden">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option selected="selected">4</option>
                                                <option>5</option>
                                                <option>6</option>
                                                <option>7</option>
                                                <option>8</option>
                                                <option>9</option>
                                                <option>10</option>
                                                <option>11</option>
                                                <option>12</option>
                                                <option>13</option>
                                                <option>14</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group form-group-select-plus">
                                            <label>Children</label>
                                            <div class="btn-group btn-group-select-num" data-toggle="buttons">
                                                <label class="btn btn-primary active">
                                                    <input type="radio" name="options" />0</label>
                                                <label class="btn btn-primary">
                                                    <input type="radio" name="options" />1</label>
                                                <label class="btn btn-primary">
                                                    <input type="radio" name="options" />2</label>
                                                <label class="btn btn-primary">
                                                    <input type="radio" name="options" />2+</label>
                                            </div>
                                            <select class="form-control hidden">
                                                <option>0</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option selected="selected">3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                <option>6</option>
                                                <option>7</option>
                                                <option>8</option>
                                                <option>9</option>
                                                <option>10</option>
                                                <option>11</option>
                                                <option>12</option>
                                                <option>13</option>
                                                <option>14</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group form-group-select-plus">
                                            <label>Rooms</label>
                                            <div class="btn-group btn-group-select-num" data-toggle="buttons">
                                                <label class="btn btn-primary active">
                                                    <input type="radio" name="options" />1</label>
                                                <label class="btn btn-primary">
                                                    <input type="radio" name="options" />2</label>
                                                <label class="btn btn-primary">
                                                    <input type="radio" name="options" />3</label>
                                                <label class="btn btn-primary">
                                                    <input type="radio" name="options" />3+</label>
                                            </div>
                                            <select class="form-control hidden">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option selected="selected">4</option>
                                                <option>5</option>
                                                <option>6</option>
                                                <option>7</option>
                                                <option>8</option>
                                                <option>9</option>
                                                <option>10</option>
                                                <option>11</option>
                                                <option>12</option>
                                                <option>13</option>
                                                <option>14</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="gap gap-small"></div>
                        <ul class="booking-list">
                            <li>
                                <a class="booking-item">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img src="img/800x600.png" alt="Image Alternative text" title="hotel PORTO BAY RIO INTERNACIONAL rooftop pool" />
                                        </div>
                                        <div class="col-md-6">
                                            <h5 class="booking-item-title">Superior Penthouse</h5>
                                            <p class="text-small">Mus cubilia hendrerit vestibulum accumsan consectetur convallis vitae nec sapien diam justo lobortis</p>
                                            <ul class="booking-item-features booking-item-features-sign clearfix">
                                                <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 3</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Children Occupancy"><i class="im im-children"></i><span class="booking-item-feature-sign">x 1</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 2</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">200</span>
                                                </li>
                                            </ul>
                                            <ul class="booking-item-features booking-item-features-small clearfix">
                                                <li rel="tooltip" data-placement="top" title="Flat Screen TV"><i class="im im-tv"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Bathtub"><i class="im im-bathtub"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Kitchen"><i class="im im-kitchen"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Patio"><i class="im im-patio"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="SPA tub"><i class="im im-spa"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Terrace"><i class="im im-terrace"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Washing Machine"><i class="im im-washing-machine"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3"><span class="booking-item-price">$357</span><span>/night</span><span class="btn btn-primary">Book</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a class="booking-item">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img src="img/800x600.png" alt="Image Alternative text" title="LHOTEL PORTO BAY SAO PAULO lobby" />
                                        </div>
                                        <div class="col-md-6">
                                            <h5 class="booking-item-title">Standard Double room</h5>
                                            <p class="text-small">Aenean lobortis tristique interdum curae luctus mattis nisl aenean diam suscipit sagittis viverra</p>
                                            <ul class="booking-item-features booking-item-features-sign clearfix">
                                                <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 2</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 1</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">450</span>
                                                </li>
                                            </ul>
                                            <ul class="booking-item-features booking-item-features-small clearfix">
                                                <li rel="tooltip" data-placement="top" title="Air Conditioning"><i class="im im-air"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Flat Screen TV"><i class="im im-tv"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Mini Bar"><i class="im im-bar"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Bathtub"><i class="im im-bathtub"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Kitchen"><i class="im im-kitchen"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Soundproof"><i class="im im-soundproof"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="SPA tub"><i class="im im-spa"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Terrace"><i class="im im-terrace"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Washing Machine"><i class="im im-washing-machine"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Pool"><i class="im im-pool"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3"><span class="booking-item-price">$182</span><span>/night</span><span class="btn btn-primary">Book</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a class="booking-item">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img src="img/800x600.png" alt="Image Alternative text" title="hotel 2" />
                                        </div>
                                        <div class="col-md-6">
                                            <h5 class="booking-item-title">Double Room with Town View</h5>
                                            <p class="text-small">Adipiscing accumsan diam taciti vulputate nec lorem ad nam parturient ultrices euismod placerat</p>
                                            <ul class="booking-item-features booking-item-features-sign clearfix">
                                                <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 3</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 2</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">670</span>
                                                </li>
                                            </ul>
                                            <ul class="booking-item-features booking-item-features-small clearfix">
                                                <li rel="tooltip" data-placement="top" title="Air Conditioning"><i class="im im-air"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Flat Screen TV"><i class="im im-tv"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Mini Bar"><i class="im im-bar"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Bathtub"><i class="im im-bathtub"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Terrace"><i class="im im-terrace"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Washing Machine"><i class="im im-washing-machine"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Pool"><i class="im im-pool"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3"><span class="booking-item-price">$471</span><span>/night</span><span class="btn btn-primary">Book</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a class="booking-item">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img src="img/800x600.png" alt="Image Alternative text" title="hotel 1" />
                                        </div>
                                        <div class="col-md-6">
                                            <h5 class="booking-item-title">Junior Suite</h5>
                                            <p class="text-small">Sit duis ad sociosqu dis nibh amet volutpat habitant rhoncus pretium non imperdiet</p>
                                            <ul class="booking-item-features booking-item-features-sign clearfix">
                                                <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 1</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Children Occupancy"><i class="im im-children"></i><span class="booking-item-feature-sign">x 1</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 1</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">390</span>
                                                </li>
                                            </ul>
                                            <ul class="booking-item-features booking-item-features-small clearfix">
                                                <li rel="tooltip" data-placement="top" title="Air Conditioning"><i class="im im-air"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Flat Screen TV"><i class="im im-tv"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Mini Bar"><i class="im im-bar"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Kitchen"><i class="im im-kitchen"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Patio"><i class="im im-patio"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Soundproof"><i class="im im-soundproof"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="SPA tub"><i class="im im-spa"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Washing Machine"><i class="im im-washing-machine"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Pool"><i class="im im-pool"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3"><span class="booking-item-price">$164</span><span>/night</span><span class="btn btn-primary">Book</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a class="booking-item">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img src="img/800x600.png" alt="Image Alternative text" title="The pool" />
                                        </div>
                                        <div class="col-md-6">
                                            <h5 class="booking-item-title">Family Suite</h5>
                                            <p class="text-small">Vel torquent penatibus torquent taciti torquent nullam vitae proin natoque ligula dui et</p>
                                            <ul class="booking-item-features booking-item-features-sign clearfix">
                                                <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 2</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Children Occupancy"><i class="im im-children"></i><span class="booking-item-feature-sign">x 2</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 2</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">520</span>
                                                </li>
                                            </ul>
                                            <ul class="booking-item-features booking-item-features-small clearfix">
                                                <li rel="tooltip" data-placement="top" title="Mini Bar"><i class="im im-bar"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Bathtub"><i class="im im-bathtub"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Kitchen"><i class="im im-kitchen"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Patio"><i class="im im-patio"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Soundproof"><i class="im im-soundproof"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="SPA tub"><i class="im im-spa"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Terrace"><i class="im im-terrace"></i>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Pool"><i class="im im-pool"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3"><span class="booking-item-price">$487</span><span>/night</span><span class="btn btn-primary">Book</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <h4>About the Hotel</h4>
                        <p class="mb30">Sodales curae vitae eros odio fermentum nisi hac sodales erat fermentum lobortis porta sed condimentum diam tortor felis ipsum natoque</p>
                        <h4>Hotel Facilities</h4>
                        <ul class="booking-item-features booking-item-features-expand mb30 clearfix">
                            <li><i class="im im-wi-fi"></i><span class="booking-item-feature-title">Wi-Fi Internet</span>
                            </li>
                            <li><i class="im im-parking"></i><span class="booking-item-feature-title">Parking</span>
                            </li>
                            <li><i class="im im-plane"></i><span class="booking-item-feature-title">Airport Transport</span>
                            </li>
                            <li><i class="im im-bus"></i><span class="booking-item-feature-title">Shuttle Bus Service</span>
                            </li>
                            <li><i class="im im-fitness"></i><span class="booking-item-feature-title">Fitness Center</span>
                            </li>
                            <li><i class="im im-pool"></i><span class="booking-item-feature-title">Pool</span>
                            </li>
                            <li><i class="im im-spa"></i><span class="booking-item-feature-title">SPA</span>
                            </li>
                            <li><i class="im im-restaurant"></i><span class="booking-item-feature-title">Restaurant</span>
                            </li>
                            <li><i class="im im-wheel-chair"></i><span class="booking-item-feature-title">Wheelchair Access</span>
                            </li>
                            <li><i class="im im-business-person"></i><span class="booking-item-feature-title">Business Center</span>
                            </li>
                            <li><i class="im im-children"></i><span class="booking-item-feature-title">Children Activites</span>
                            </li>
                            <li><i class="im im-casino"></i><span class="booking-item-feature-title">Casino & Gambling</span>
                            </li>
                            <li><i class="im im-bar"></i><span class="booking-item-feature-title">Bar/Lounge</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <h3 class="mb20">Hotel Reviews</h3>
                <div class="row row-wrap">
                    <div class="col-md-8">
                        <ul class="booking-item-reviews list">
                            <li>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="booking-item-review-person">
                                            <a class="booking-item-review-person-avatar round" href="#">
                                                <img src="img/70x70.png" alt="Image Alternative text" title="AMaze" />
                                            </a>
                                            <p class="booking-item-review-person-name"><a href="#">John Doe</a>
                                            </p>
                                            <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">67 Reviews</a></small>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="booking-item-review-content">
                                            <h5>"Consectetur aptent proin nostra iaculis"</h5>
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                            </ul>
                                            <p>Gravida nibh lectus in diam a penatibus nulla nunc dapibus conubia aliquam faucibus semper diam commodo faucibus dapibus parturient sapien egestas varius tortor nec cubilia habitasse quisque justo<span class="booking-item-review-more"> Pellentesque consequat neque sagittis tellus lobortis donec penatibus himenaeos condimentum vivamus malesuada enim eros pulvinar et condimentum ornare sociosqu libero aenean maecenas congue a non magna tristique in vel tincidunt arcu nisi orci imperdiet urna augue ridiculus duis habitasse sem varius blandit per erat elit</span>
                                            </p>
                                            <div class="booking-item-review-more-content">
                                                <p>Eget primis in et habitant tellus pharetra consectetur per varius blandit himenaeos turpis etiam blandit convallis euismod proin parturient vulputate mi parturient elementum ante sociosqu tellus sociis torquent vehicula proin tincidunt nostra auctor blandit id sem ultricies etiam</p>
                                                <p>Risus nec primis cum amet massa donec aliquam pellentesque platea natoque sagittis nec pretium mollis quam etiam porttitor fusce quisque primis dui phasellus diam inceptos hac sagittis sit elit primis iaculis arcu</p>
                                                <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <ul class="list booking-item-raiting-summary-list">
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Sleep</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Location</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Service</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <ul class="list booking-item-raiting-summary-list">
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Clearness</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Rooms</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o text-gray"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                            </div>
                                            <p class="booking-item-review-rate">Was this review helpful?
                                                <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 16</b>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="booking-item-review-person">
                                            <a class="booking-item-review-person-avatar round" href="#">
                                                <img src="img/70x70.png" alt="Image Alternative text" title="Bubbles" />
                                            </a>
                                            <p class="booking-item-review-person-name"><a href="#">Minnie Aviles</a>
                                            </p>
                                            <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">34 Reviews</a></small>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="booking-item-review-content">
                                            <h5>"Quam justo per primis tempus ad iaculis"</h5>
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                            </ul>
                                            <p>Cursus condimentum nullam pretium dui id sit lacus duis dignissim primis potenti aliquam malesuada ullamcorper euismod volutpat risus luctus id varius<span class="booking-item-review-more"> Volutpat adipiscing porttitor egestas nisl nunc luctus phasellus nibh tristique lacinia penatibus justo urna quis accumsan arcu faucibus dis at vivamus convallis venenatis pharetra cum viverra ridiculus dictum aliquam vitae sollicitudin lacinia nisl platea turpis</span>
                                            </p>
                                            <div class="booking-item-review-more-content">
                                                <p>Quis feugiat duis cursus accumsan diam cursus gravida montes nostra nostra pellentesque dictumst nostra cursus sociosqu euismod orci tristique blandit porta per laoreet convallis purus cursus sodales dignissim euismod class primis viverra penatibus urna</p>
                                                <p>Dictumst primis praesent odio scelerisque cubilia sapien placerat per lectus proin amet in habitant tincidunt dictum aptent blandit blandit auctor erat hendrerit placerat diam blandit interdum mi litora cum ut vel integer metus cursus scelerisque vitae sem sem lacinia nunc condimentum commodo pellentesque habitant vulputate pulvinar suspendisse nibh neque velit suscipit gravida ligula lorem a aptent potenti</p>
                                                <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <ul class="list booking-item-raiting-summary-list">
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Sleep</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Location</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Service</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <ul class="list booking-item-raiting-summary-list">
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Clearness</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Rooms</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o text-gray"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                            </div>
                                            <p class="booking-item-review-rate">Was this review helpful?
                                                <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 11</b>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="booking-item-review-person">
                                            <a class="booking-item-review-person-avatar round" href="#">
                                                <img src="img/70x70.png" alt="Image Alternative text" title="Me with the Uke" />
                                            </a>
                                            <p class="booking-item-review-person-name"><a href="#">Cyndy Naquin</a>
                                            </p>
                                            <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">70 Reviews</a></small>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="booking-item-review-content">
                                            <h5>"Malesuada cras porttitor varius quisque non quis"</h5>
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                            </ul>
                                            <p>Eleifend lorem erat dictum dis habitasse lacinia leo ullamcorper nullam venenatis mi vitae ac dolor tincidunt maecenas posuere a magna aenean<span class="booking-item-review-more"> Ligula platea sem posuere lobortis conubia senectus condimentum litora orci vivamus feugiat pellentesque blandit est velit risus sed cubilia class aliquam odio neque fringilla cubilia ultricies ante quisque fringilla mauris mattis vivamus felis quis lectus augue faucibus magna sem ornare amet ante bibendum sagittis pretium facilisi egestas class auctor mus potenti parturient faucibus placerat mattis arcu curae adipiscing blandit ipsum sociis ipsum interdum facilisis ante dis vel</span>
                                            </p>
                                            <div class="booking-item-review-more-content">
                                                <p>Senectus aenean egestas pharetra nascetur magnis sociosqu aptent nullam pellentesque tempor arcu nisl elit placerat curae neque lobortis congue faucibus cursus ante magnis a lectus donec phasellus dictum a pellentesque accumsan fringilla varius nullam fusce justo suspendisse senectus vulputate turpis sodales lobortis sociis ridiculus magna euismod</p>
                                                <p>Dictumst lorem torquent magnis turpis volutpat orci sem gravida sagittis sociosqu libero feugiat sit auctor cubilia donec netus laoreet class sit aliquam metus adipiscing mi nunc lacus curae cursus habitasse quam dui at per nec integer bibendum montes malesuada libero nam libero nostra nec gravida habitasse</p>
                                                <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <ul class="list booking-item-raiting-summary-list">
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Sleep</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Location</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Service</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <ul class="list booking-item-raiting-summary-list">
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Clearness</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Rooms</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                            </div>
                                            <p class="booking-item-review-rate">Was this review helpful?
                                                <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 8</b>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="booking-item-review-person">
                                            <a class="booking-item-review-person-avatar round" href="#">
                                                <img src="img/70x70.png" alt="Image Alternative text" title="Gamer Chick" />
                                            </a>
                                            <p class="booking-item-review-person-name"><a href="#">Carol Blevins</a>
                                            </p>
                                            <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">125 Reviews</a></small>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="booking-item-review-content">
                                            <h5>"Metus malesuada ridiculus"</h5>
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                            </ul>
                                            <p>Praesent fames laoreet placerat metus nascetur commodo lectus eleifend inceptos praesent tellus semper imperdiet ac felis pulvinar nunc dictumst odio vitae primis vestibulum curae volutpat rhoncus nec proin quam gravida proin potenti primis morbi parturient ultricies torquent viverra aptent pretium<span class="booking-item-review-more"> Posuere proin curabitur vehicula curabitur inceptos phasellus vehicula placerat at sociosqu nam etiam fringilla hac convallis integer facilisi libero massa adipiscing montes sodales aenean non dapibus purus mollis convallis risus tempus id arcu leo lobortis integer hac rutrum egestas vel</span>
                                            </p>
                                            <div class="booking-item-review-more-content">
                                                <p>Malesuada tellus himenaeos suspendisse ligula faucibus nulla luctus platea dis dolor ornare dapibus consectetur sit praesent hendrerit dapibus potenti metus enim</p>
                                                <p>Varius neque ipsum eu mollis parturient eleifend leo montes orci feugiat dis aptent ridiculus molestie mattis penatibus quisque dis suscipit lorem sit dapibus dignissim at placerat rutrum vulputate mollis mus tincidunt elementum maecenas curae sit imperdiet a potenti lectus interdum phasellus varius bibendum rhoncus nostra aptent neque lectus inceptos</p>
                                                <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <ul class="list booking-item-raiting-summary-list">
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Sleep</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o text-gray"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Location</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o text-gray"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Service</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o text-gray"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <ul class="list booking-item-raiting-summary-list">
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Clearness</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Rooms</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o text-gray"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                            </div>
                                            <p class="booking-item-review-rate">Was this review helpful?
                                                <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 16</b>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="booking-item-review-person">
                                            <a class="booking-item-review-person-avatar round" href="#">
                                                <img src="img/70x70.png" alt="Image Alternative text" title="Chiara" />
                                            </a>
                                            <p class="booking-item-review-person-name"><a href="#">Cheryl Gustin</a>
                                            </p>
                                            <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">105 Reviews</a></small>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="booking-item-review-content">
                                            <h5>"Risus facilisi torquent consequat fermentum arcu"</h5>
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                            </ul>
                                            <p>Blandit imperdiet ipsum egestas massa eu et nullam condimentum integer non non hac sociosqu parturient adipiscing elementum dignissim congue donec dui montes donec ac elementum malesuada penatibus rhoncus sed cum porta feugiat suspendisse blandit adipiscing<span class="booking-item-review-more"> Aliquet tincidunt integer taciti proin sollicitudin platea varius rhoncus dui aptent litora vivamus vel augue curae potenti molestie suscipit elementum sem ultrices vivamus lacus nostra nibh morbi vehicula vitae massa curae massa eget cubilia tortor fringilla egestas litora hac cubilia fames ut orci cum faucibus molestie</span>
                                            </p>
                                            <div class="booking-item-review-more-content">
                                                <p>Fusce augue netus pretium molestie cum viverra lorem sociis auctor sociosqu erat feugiat auctor volutpat aliquam nisl taciti sapien etiam rhoncus pharetra blandit velit integer duis dui mollis nisl potenti ad</p>
                                                <p>Vulputate vitae tempor sodales sapien nibh ultricies molestie tristique at neque lacus sapien luctus auctor imperdiet lacus lacus commodo proin eu accumsan torquent fermentum fermentum senectus dapibus magnis accumsan vehicula cum commodo fringilla viverra montes lobortis pulvinar aenean neque ligula donec per vitae integer donec ac varius curabitur porttitor nullam donec ante cursus nunc viverra ut nam non vulputate curabitur hendrerit pellentesque ligula mus mollis</p>
                                                <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <ul class="list booking-item-raiting-summary-list">
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Sleep</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Location</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Service</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <ul class="list booking-item-raiting-summary-list">
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Clearness</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Rooms</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                            </div>
                                            <p class="booking-item-review-rate">Was this review helpful?
                                                <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 20</b>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="booking-item-review-person">
                                            <a class="booking-item-review-person-avatar round" href="#">
                                                <img src="img/70x70.png" alt="Image Alternative text" title="Afro" />
                                            </a>
                                            <p class="booking-item-review-person-name"><a href="#">Joe Smith</a>
                                            </p>
                                            <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">62 Reviews</a></small>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="booking-item-review-content">
                                            <h5>"Himenaeos class lorem"</h5>
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                            </ul>
                                            <p>Nam sociis congue quis imperdiet ornare elementum quam magnis turpis platea facilisis sollicitudin dolor dignissim pulvinar ultrices nullam vel ultricies phasellus risus porttitor dignissim nibh purus ornare imperdiet<span class="booking-item-review-more"> Nullam convallis mattis sollicitudin enim ad mauris lacus lectus a iaculis lorem pellentesque dictum felis semper tellus cum ante sed mus vulputate scelerisque eu per fermentum etiam nisi platea dui posuere nibh netus eros sed nisl senectus natoque sapien maecenas himenaeos proin in natoque porta sociis semper posuere</span>
                                            </p>
                                            <div class="booking-item-review-more-content">
                                                <p>Rhoncus lorem rutrum elit arcu purus mauris adipiscing molestie elementum primis id per ante orci dapibus justo ridiculus vivamus primis est massa vel turpis diam</p>
                                                <p>Sociosqu eu aliquet non suspendisse dignissim vivamus mollis dapibus in arcu lectus taciti congue cras etiam mi aptent montes mauris eros ligula conubia convallis pellentesque adipiscing eleifend netus semper viverra posuere sollicitudin felis ante tincidunt inceptos eros lectus nulla dignissim elit tempor enim porta hac nec nulla torquent dis erat augue elit suscipit cubilia ligula potenti ultrices vehicula consectetur risus magnis tincidunt tempus integer dapibus placerat congue</p>
                                                <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <ul class="list booking-item-raiting-summary-list">
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Sleep</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o text-gray"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Location</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Service</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <ul class="list booking-item-raiting-summary-list">
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Clearness</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Rooms</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                            </div>
                                            <p class="booking-item-review-rate">Was this review helpful?
                                                <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 5</b>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="booking-item-review-person">
                                            <a class="booking-item-review-person-avatar round" href="#">
                                                <img src="img/70x70.png" alt="Image Alternative text" title="Good job" />
                                            </a>
                                            <p class="booking-item-review-person-name"><a href="#">Ava McDonald</a>
                                            </p>
                                            <p class="booking-item-review-person-loc">Palm Beach, FL</p><small><a href="#">128 Reviews</a></small>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="booking-item-review-content">
                                            <h5>"Arcu aliquet nam orci potenti platea eleifend"</h5>
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                            </ul>
                                            <p>Semper sociis eget ultrices maecenas imperdiet elementum fringilla viverra sodales semper porttitor cursus diam mus pulvinar hendrerit sollicitudin mus per lacus sociis inceptos dolor posuere nisi dignissim nam aliquam congue mauris lobortis quis sociosqu lorem cras<span class="booking-item-review-more"> Pretium quam parturient euismod et fermentum curabitur cubilia elit mattis nascetur ipsum vulputate sapien viverra fermentum habitasse cras ac nibh phasellus accumsan praesent laoreet scelerisque id potenti nec auctor praesent class eget fringilla volutpat leo consectetur urna per tellus</span>
                                            </p>
                                            <div class="booking-item-review-more-content">
                                                <p>Libero porttitor praesent vel velit vulputate nostra sapien suscipit pellentesque felis viverra suscipit adipiscing facilisis nostra dictumst vehicula mi euismod nam quisque lorem blandit dis fermentum congue nostra semper venenatis tortor velit odio molestie sed potenti id iaculis fames vivamus erat fringilla himenaeos fusce vulputate purus litora</p>
                                                <p>Torquent sem litora morbi eleifend justo sit congue ligula aliquet placerat commodo curae in himenaeos vulputate lacinia porta pulvinar rhoncus sollicitudin pharetra conubia tempor molestie mollis id facilisis at accumsan sodales malesuada montes faucibus hac leo nam curabitur est nostra dolor quis justo vestibulum auctor</p>
                                                <p class="text-small mt20">Stayed March 2014, traveled as a couple</p>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <ul class="list booking-item-raiting-summary-list">
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Sleep</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Location</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Service</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <ul class="list booking-item-raiting-summary-list">
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Clearness</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li>
                                                                <div class="booking-item-raiting-list-title">Rooms</div>
                                                                <ul class="icon-group booking-item-rating-stars">
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                    <li><i class="fa fa-smile-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                            </div>
                                            <p class="booking-item-review-rate">Was this review helpful?
                                                <a class="fa fa-thumbs-o-up box-icon-inline round" href="#"></a><b class="text-color"> 15</b>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="row wrap">
                            <div class="col-md-5">
                                <p><small>1403 reviews on this hotel. &nbsp;&nbsp;Showing 1 to 7</small>
                                </p>
                            </div>
                            <div class="col-md-7">
                                <ul class="pagination">
                                    <li class="active"><a href="#">1</a>
                                    </li>
                                    <li><a href="#">2</a>
                                    </li>
                                    <li><a href="#">3</a>
                                    </li>
                                    <li><a href="#">4</a>
                                    </li>
                                    <li><a href="#">5</a>
                                    </li>
                                    <li><a href="#">6</a>
                                    </li>
                                    <li><a href="#">7</a>
                                    </li>
                                    <li class="dots">...</li>
                                    <li><a href="#">43</a>
                                    </li>
                                    <li class="next"><a href="#">Next Page</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="gap gap-small"></div>
                        <div class="box bg-gray">
                            <h3>Write a Review</h3>
                            <form>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>Review Title</label>
                                            <input class="form-control" type="text" />
                                        </div>
                                        <div class="form-group">
                                            <label>Review Text</label>
                                            <textarea class="form-control" rows="6"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="list booking-item-raiting-summary-list stats-list-select">
                                            <li>
                                                <div class="booking-item-raiting-list-title">Sleep</div>
                                                <ul class="icon-group booking-item-rating-stars">
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <div class="booking-item-raiting-list-title">Location</div>
                                                <ul class="icon-group booking-item-rating-stars">
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <div class="booking-item-raiting-list-title">Service</div>
                                                <ul class="icon-group booking-item-rating-stars">
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <div class="booking-item-raiting-list-title">Clearness</div>
                                                <ul class="icon-group booking-item-rating-stars">
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <div class="booking-item-raiting-list-title">Rooms</div>
                                                <ul class="icon-group booking-item-rating-stars">
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                    <li><i class="fa fa-smile-o"></i>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                        <input class="btn btn-primary" type="submit" value="Leave a Review" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h4>Hotels Near InterContinental New York Barclay</h4>
                        <ul class="booking-list">
                            <li>
                                <div class="booking-item booking-item-small">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <img src="img/800x600.png" alt="Image Alternative text" title="hotel 2" />
                                        </div>
                                        <div class="col-xs-5">
                                            <h5 class="booking-item-title">New York Hilton Midtown</h5>
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-o"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-xs-3"><span class="booking-item-price-from">from</span><span class="booking-item-price">$319</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="booking-item booking-item-small">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <img src="img/800x600.png" alt="Image Alternative text" title="hotel PORTO BAY SERRA GOLF living room" />
                                        </div>
                                        <div class="col-xs-5">
                                            <h5 class="booking-item-title">Waldorf Astoria New York</h5>
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-half-empty"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-xs-3"><span class="booking-item-price-from">from</span><span class="booking-item-price">$288</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="booking-item booking-item-small">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <img src="img/800x600.png" alt="Image Alternative text" title="hotel THE CLIFF BAY spa suite" />
                                        </div>
                                        <div class="col-xs-5">
                                            <h5 class="booking-item-title">Wellington Hotel</h5>
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-o"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-xs-3"><span class="booking-item-price-from">from</span><span class="booking-item-price">$155</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="booking-item booking-item-small">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <img src="img/800x600.png" alt="Image Alternative text" title="hotel PORTO BAY RIO INTERNACIONAL rooftop pool" />
                                        </div>
                                        <div class="col-xs-5">
                                            <h5 class="booking-item-title">Warwick New York Hotel</h5>
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-xs-3"><span class="booking-item-price-from">from</span><span class="booking-item-price">$380</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="booking-item booking-item-small">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <img src="img/800x600.png" alt="Image Alternative text" title="hotel PORTO BAY LIBERDADE" />
                                        </div>
                                        <div class="col-xs-5">
                                            <h5 class="booking-item-title">Holiday Inn Express Kennedy Airport</h5>
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-o"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-xs-3"><span class="booking-item-price-from">from</span><span class="booking-item-price">$202</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="booking-item booking-item-small">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <img src="img/800x600.png" alt="Image Alternative text" title="The pool" />
                                        </div>
                                        <div class="col-xs-5">
                                            <h5 class="booking-item-title">Grand Hyatt New York</h5>
                                            <ul class="icon-group booking-item-rating-stars">
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star"></i>
                                                </li>
                                                <li><i class="fa fa-star-o"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-xs-3"><span class="booking-item-price-from">from</span><span class="booking-item-price">$221</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="gap gap-small"></div>
        </div>



        <?php
		
		include "includes/files/footer.php";
		
		?>


        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script src="js/nicescroll.js"></script>
        <script src="js/dropit.js"></script>
        <script src="js/ionrangeslider.js"></script>
        <script src="js/icheck.js"></script>
        <script src="js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="js/typeahead.js"></script>
        <script src="js/card-payment.js"></script>
        <script src="js/magnific.js"></script>
        <script src="js/owl-carousel.js"></script>
        <script src="js/fitvids.js"></script>
        <script src="js/tweet.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/gridrotator.js"></script>
        <script src="js/custom.js"></script>
    </div>
</body>

</html>


