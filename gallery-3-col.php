<!DOCTYPE HTML>
<html>

<?php
		
		include "includes/files/header_links.php";
		
		?>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        
		 <?php
		
		include "includes/files/page_header.php";
		
		?>

        <div class="container">
            <h1 class="page-title">Gallery 3 Columns</h1>
        </div>




        <div class="container">
            <div id="popup-gallery">
                <div class="row row-col-gap">
                    <div class="col-md-4">
                        <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                            <img src="img/800x600.png" alt="Image Alternative text" title="Gaviota en el Top" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                            <img src="img/800x600.png" alt="Image Alternative text" title="Sydney Harbour" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                            <img src="img/800x600.png" alt="Image Alternative text" title="Street" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a class="hover-img popup-gallery-image" href="img/400x300.png" data-effect="mfp-zoom-out">
                            <img src="img/400x300.png" alt="Image Alternative text" title="the journey home" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                            <img src="img/800x600.png" alt="Image Alternative text" title="lack of blue depresses me" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                            <img src="img/800x600.png" alt="Image Alternative text" title="waipio valley" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                            <img src="img/800x600.png" alt="Image Alternative text" title="the best mode of transport here in maldives" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                            <img src="img/800x600.png" alt="Image Alternative text" title="Upper Lake in New York Central Park" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                            <img src="img/800x600.png" alt="Image Alternative text" title="Viva Las Vegas" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                            <img src="img/800x600.png" alt="Image Alternative text" title="El inevitable paso del tiempo" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                            <img src="img/800x600.png" alt="Image Alternative text" title="new york at an angle" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                            <img src="img/800x600.png" alt="Image Alternative text" title="196_365" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>



        <div class="gap"></div>
        
		<?php
		
		include "includes/files/footer.php";
		
		?>


        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script src="js/nicescroll.js"></script>
        <script src="js/dropit.js"></script>
        <script src="js/ionrangeslider.js"></script>
        <script src="js/icheck.js"></script>
        <script src="js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="js/typeahead.js"></script>
        <script src="js/card-payment.js"></script>
        <script src="js/magnific.js"></script>
        <script src="js/owl-carousel.js"></script>
        <script src="js/fitvids.js"></script>
        <script src="js/tweet.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/gridrotator.js"></script>
        <script src="js/custom.js"></script>
    </div>
</body>

</html>


