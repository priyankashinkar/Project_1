<!DOCTYPE HTML>
<html>

<?php
		
		include "includes/files/header_links.php";
		
		?>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        
		 <?php
		
		include "includes/files/page_header.php";
		
		?>

        <div class="container">
            <h1 class="page-title">Magnific Lightbox Effects</h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <aside class="sidebar-left">
                        <ul class="nav nav-pills nav-stacked nav-side mb30">

                            <li><a href="feature-typography.html">Typography</a>
                            </li>
                            <li><a href="feature-icons.html">Icons</a>
                            </li>
                            <li><a href="feature-forms.html">Forms</a>
                            </li>
                            <li><a href="feature-icon-effects.html">Icon Effects</a>
                            </li>
                            <li><a href="feature-elements.html">Elements</a>
                            </li>
                            <li><a href="feature-grid.html">Grid</a>
                            </li>
                            <li><a href="feature-hovers.html">Hover effects</a>
                            </li>
                            <li class="active"><a href="feature-lightbox.html">Lightbox</a>
                            </li>
                            <li><a href="feature-media.html">Media</a>
                            </li>
                        </ul>
                    </aside>
                </div>
                <div class="col-md-9">
                    <div class="row row-wrap">
                        <div class="col-md-3">
                            <h5>Default lightbox</h5>
                            <a class="hover-img popup-image" href="img/800x600.png">
                                <img src="img/800x600.png" alt="Image Alternative text" title="LHOTEL PORTO BAY SAO PAULO suite lhotel living room" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                            </a>
                        </div>
                    </div>
                    <div class="gap gap-small"></div>
                    <h3>Animation effects</h3>
                    <div class="row row-wrap">
                        <div class="col-md-3">
                            <h5>Zoom out</h5>
                            <a class="hover-img popup-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                <img src="img/800x600.png" alt="Image Alternative text" title="LHOTEL PORTO BAY SAO PAULO lobby" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <h5>Fade</h5>
                            <a class="hover-img popup-image" href="img/800x600.png" data-effect="mfp-fade">
                                <img src="img/800x600.png" alt="Image Alternative text" title="LHOTEL PORTO BAY SAO PAULO luxury suite" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <h5>Move horizontal</h5>
                            <a class="hover-img popup-image" href="img/800x600.png" data-effect="mfp-move-horizontal">
                                <img src="img/800x600.png" alt="Image Alternative text" title="Plunklock live in Cologne" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <h5>Move from top</h5>
                            <a class="hover-img popup-image" href="img/800x600.png" data-effect="mfp-move-from-top">
                                <img src="img/800x600.png" alt="Image Alternative text" title="Me with the Uke" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                            </a>
                        </div>
                    </div>
                    <div class="gap gap-small"></div>
                    <div class="row row-wrap">
                        <div class="col-md-3">
                            <h5>Zoom in</h5>
                            <a class="hover-img popup-image" href="img/800x600.png" data-effect="mfp-zoom-in">
                                <img src="img/800x600.png" alt="Image Alternative text" title="new york at an angle" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <h5>Newspaper</h5>
                            <a class="hover-img popup-image" href="img/800x600.png" data-effect="mfp-newspaper">
                                <img src="img/800x600.png" alt="Image Alternative text" title="Pictures at the museum" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <h5>3D unfold</h5>
                            <a class="hover-img popup-image" href="img/800x600.png" data-effect="mfp-3d-unfold">
                                <img src="img/800x600.png" alt="Image Alternative text" title="AMaze" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                            </a>
                        </div>
                    </div>
                    <div class="gap gap-small"></div>
                    <h3>Gallery</h3>
                    <div class="row row-wrap" id="popup-gallery">
                        <div class="col-md-3">
                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                <img src="img/800x600.png" alt="Image Alternative text" title="Old No7" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                <img src="img/800x600.png" alt="Image Alternative text" title="Bubbles" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                <img src="img/800x600.png" alt="Image Alternative text" title="Food is Pride" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                                <img src="img/800x600.png" alt="Image Alternative text" title="Spidy" /><i class="fa fa-plus round box-icon-small hover-icon i round"></i>
                            </a>
                        </div>
                    </div>
                    <div class="gap gap-small"></div>
                    <h3>Non Image Content</h3>
                    <a class="popup-text" href="#small-dialog" data-effect="mfp-zoom-out">Modal HTML block</a> 
                    <div id="small-dialog" class="mfp-with-anim mfp-hide mfp-dialog">
                        <h4>Dialog example</h4>
                        <p>This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look and feel of finished, typeset text. Only for show. He who searches for meaning here will be sorely disappointed.</p>
                    </div>
                    <br>
                    <br>
                    <h4>Iframe Content</h4>
                    <a class="popup-iframe" inline_comment="lightbox" href="https://vimeo.com/45830194" data-effect="mfp-fade">Vimeo video</a> 
                    <br>
                    <a class="popup-iframe" inline_comment="lightbox" href="http://www.youtube.com/watch?v=0O2aH4XLbto" data-effect="mfp-fade">Youtube Video</a> 
                    <br>
                    <a class="popup-iframe" inline_comment="lightbox" href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&hl=en&t=v&hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" data-effect="mfp-fade">Google Map</a>
                </div>
            </div>
        </div>



        <div class="gap"></div>
        
		<?php
		
		include "includes/files/footer.php";
		
		?>


        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script src="js/nicescroll.js"></script>
        <script src="js/dropit.js"></script>
        <script src="js/ionrangeslider.js"></script>
        <script src="js/icheck.js"></script>
        <script src="js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="js/typeahead.js"></script>
        <script src="js/card-payment.js"></script>
        <script src="js/magnific.js"></script>
        <script src="js/owl-carousel.js"></script>
        <script src="js/fitvids.js"></script>
        <script src="js/tweet.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/gridrotator.js"></script>
        <script src="js/custom.js"></script>
    </div>
</body>

</html>


