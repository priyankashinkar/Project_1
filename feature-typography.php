<!DOCTYPE HTML>
<html>

<?php
		
		include "includes/files/header_links.php";
		
		?>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        
		 <?php
		
		include "includes/files/page_header.php";
		
		?>

        <div class="container">
            <h1 class="page-title">Typography</h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <aside class="sidebar-left">
                        <ul class="nav nav-pills nav-stacked nav-side mb30">

                            <li class="active"><a href="feature-typography.html">Typography</a>
                            </li>
                            <li><a href="feature-icons.html">Icons</a>
                            </li>
                            <li><a href="feature-forms.html">Forms</a>
                            </li>
                            <li><a href="feature-icon-effects.html">Icon Effects</a>
                            </li>
                            <li><a href="feature-elements.html">Elements</a>
                            </li>
                            <li><a href="feature-grid.html">Grid</a>
                            </li>
                            <li><a href="feature-hovers.html">Hover effects</a>
                            </li>
                            <li><a href="feature-lightbox.html">Lightbox</a>
                            </li>
                            <li><a href="feature-media.html">Media</a>
                            </li>
                        </ul>
                    </aside>
                </div>
                <div class="col-md-9">
                    <h5>Headers</h5>
                    <h1>h1 Tempus duis phasellus</h1>
                    <h2>h2 Tellus leo commodo lacinia</h2>
                    <h3>h3 Primis libero facilisis praesent</h3>
                    <h4>h4 Sociis malesuada morbi ullamcorper</h4>
                    <h5>h5 Parturient pharetra at sodales</h5>
                    <hr>
                    <h5>Normal paragraph</h5>
                    <p>Cursus nec nam suspendisse sit facilisi erat sed non lacinia tellus adipiscing viverra arcu nullam habitasse ultricies scelerisque elementum cubilia torquent primis ultricies elementum suscipit leo interdum vivamus euismod amet turpis donec curabitur consectetur cras et ultricies quis felis accumsan natoque nam dapibus risus mus nulla turpis integer suscipit mi</p>
                    <hr>
                    <h5>Small paragraph</h5>
                    <p><small>Enim elementum urna id lacinia convallis quam rhoncus cubilia justo sapien sagittis nullam vehicula adipiscing aliquam netus aptent tristique nisi enim nibh integer auctor fusce tristique a mus arcu sit pellentesque nunc cubilia potenti sodales interdum scelerisque inceptos mi lobortis quam eleifend diam turpis duis aptent quisque feugiat ullamcorper dui</small>
                    </p>
                    <hr>
                    <h5>Big paragraph</h5>
                    <p><span class="text-lg">Quam sem at morbi consectetur velit id interdum fermentum suspendisse class nullam fusce consectetur turpis nam nunc sem ante amet diam mollis eleifend lacinia lacus integer placerat montes iaculis vulputate consectetur quam luctus inceptos quam tempus porttitor duis fames eu mattis montes vulputate sed tincidunt vestibulum montes rutrum facilisi neque</span>
                    </p>
                    <hr>
                    <h5>Italic paragraph</h5>
                    <p><em>Diam arcu porta volutpat nibh volutpat nascetur et aenean interdum tellus aliquam dis pellentesque augue hendrerit faucibus netus curae suscipit commodo mollis neque nisi risus habitasse felis dis arcu iaculis sociis proin dapibus ligula class sem sit urna natoque metus elit orci eleifend libero cum varius mus porttitor quam per</em>
                    </p>
                    <hr>
                    <h5>Strong paragraph</h5>
                    <p><strong>Magnis taciti ridiculus cursus mi aliquam lacus etiam nulla scelerisque libero habitant elit eros amet egestas hendrerit accumsan ut elit libero cubilia fusce erat luctus elit vitae nisi tristique ipsum arcu conubia posuere enim felis dapibus habitant sodales venenatis nec scelerisque rutrum potenti porta molestie cursus sapien hendrerit mattis neque</strong>
                    </p>
                    <hr>
                    <h5>Blockqoute</h5>
                    <blockquote>
                        <p>Ultrices ut donec cras fames elit nibh interdum pharetra odio convallis massa elit velit sagittis accumsan pulvinar montes aliquet adipiscing himenaeos enim suscipit cras suscipit justo porta lectus justo tincidunt molestie molestie risus a sodales massa cum nisl neque vel enim amet eget vestibulum faucibus curae quam cum aliquam facilisis</p>
                        <footer>Someone famous in <cite title="Source Title"><a href="#">Source Title</a></cite>
                        </footer>
                    </blockquote>
                </div>
            </div>
        </div>



        <div class="gap"></div>
        <footer id="main-footer">
            <div class="container">
                <div class="row row-wrap">
                    <div class="col-md-3">
                        <a class="logo" href="index.html">
                            <img src="img/logo-invert.png" alt="Image Alternative text" title="Image Title" />
                        </a>
                        <p class="mb20">Booking, reviews and advices on hotels, resorts, flights, vacation rentals, travel packages, and lots more!</p>
                        <ul class="list list-horizontal list-space">
                            <li>
                                <a class="fa fa-facebook box-icon-normal round animate-icon-bottom-to-top" href="#"></a>
                            </li>
                            <li>
                                <a class="fa fa-twitter box-icon-normal round animate-icon-bottom-to-top" href="#"></a>
                            </li>
                            <li>
                                <a class="fa fa-google-plus box-icon-normal round animate-icon-bottom-to-top" href="#"></a>
                            </li>
                            <li>
                                <a class="fa fa-linkedin box-icon-normal round animate-icon-bottom-to-top" href="#"></a>
                            </li>
                            <li>
                                <a class="fa fa-pinterest box-icon-normal round animate-icon-bottom-to-top" href="#"></a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-3">
                        <h4>Newsletter</h4>
                        <form>
                            <label>Enter your E-mail Address</label>
                            <input type="text" class="form-control">
                            <p class="mt5"><small>*We Never Send Spam</small>
                            </p>
                            <input type="submit" class="btn btn-primary" value="Subscribe">
                        </form>
                    </div>
                    <div class="col-md-2">
                        <ul class="list list-footer">
                            <li><a href="#">About US</a>
                            </li>
                            <li><a href="#">Press Centre</a>
                            </li>
                            <li><a href="#">Best Price Guarantee</a>
                            </li>
                            <li><a href="#">Travel News</a>
                            </li>
                            <li><a href="#">Jobs</a>
                            </li>
                            <li><a href="#">Privacy Policy</a>
                            </li>
                            <li><a href="#">Terms of Use</a>
                            </li>
                            <li><a href="#">Feedback</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h4>Have Questions?</h4>
                        <h4 class="text-color">+1-202-555-0173</h4>
                        <h4><a href="#" class="text-color">support@traveler.com</a></h4>
                        <p>24/7 Dedicated Customer Support</p>
                    </div>

                </div>
            </div>
        </footer>

        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script src="js/nicescroll.js"></script>
        <script src="js/dropit.js"></script>
        <script src="js/ionrangeslider.js"></script>
        <script src="js/icheck.js"></script>
        <script src="js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="js/typeahead.js"></script>
        <script src="js/card-payment.js"></script>
        <script src="js/magnific.js"></script>
        <script src="js/owl-carousel.js"></script>
        <script src="js/fitvids.js"></script>
        <script src="js/tweet.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/gridrotator.js"></script>
        <script src="js/custom.js"></script>
    </div>
</body>

</html>


