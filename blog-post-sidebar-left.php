<!DOCTYPE HTML>
<html>
<?php
		
		include "includes/files/header_links.php";
		
		?>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        
		 <?php
		
		include "includes/files/page_header.php";
		
		?>

        <div class="container">
            <h1 class="page-title">Traveler Blog</h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <aside class="sidebar-left">
                        <div class="sidebar-widget">
                            <div class="Form">
                                <input class="form-control" placeholder="Search..." type="text" />
                            </div>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Categories</h4>
                            <ul class="icon-list list-category">
                                <li><a href="#"><i class="fa fa-angle-right"></i>Photos <small >(72)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Vacation <small >(69)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Flights <small >(97)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Travel Advices <small >(65)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Trending Now <small >(99)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Hotels <small >(77)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Places to Go <small >(75)</small></a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>Travel Stories <small >(59)</small></a>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Popular Posts</h4>
                            <ul class="thumb-list">
                                <li>
                                    <a href="#">
                                        <img src="img/70x70.png" alt="Image Alternative text" title="Viva Las Vegas" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">Jul 18, 2014</p>
                                        <h5 class="thumb-list-item-title"><a href="#">Velit penatibus</a></h5>
                                        <p class="thumb-list-item-desciption">Elit gravida neque mollis purus</p>
                                    </div>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/70x70.png" alt="Image Alternative text" title="4 Strokes of Fun" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">Jul 18, 2014</p>
                                        <h5 class="thumb-list-item-title"><a href="#">Vivamus habitasse</a></h5>
                                        <p class="thumb-list-item-desciption">Iaculis nullam cras consectetur cum</p>
                                    </div>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/70x70.png" alt="Image Alternative text" title="Cup on red" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">Jul 18, 2014</p>
                                        <h5 class="thumb-list-item-title"><a href="#">Luctus metus</a></h5>
                                        <p class="thumb-list-item-desciption">Dui feugiat integer hac maecenas</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Twitter Feed</h4>
                            <div class="twitter" id="twitter"></div>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Recent Comments</h4>
                            <ul class="thumb-list thumb-list-right">
                                <li>
                                    <a href="#">
                                        <img class="rounded" src="img/70x70.png" alt="Image Alternative text" title="Afro" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">5 minutes ago</p>
                                        <h4 class="thumb-list-item-title"><a href="#">Joseph Hudson</a></h4>
                                        <p class="thumb-list-item-desciption">Elit morbi magna montes felis venenatis purus...</p>
                                    </div>
                                </li>
                                <li>
                                    <a href="#">
                                        <img class="rounded" src="img/70x70.png" alt="Image Alternative text" title="Gamer Chick" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">10 minutes ago</p>
                                        <h4 class="thumb-list-item-title"><a href="#">Cheryl Gustin</a></h4>
                                        <p class="thumb-list-item-desciption">Parturient parturient vel nisl metus ultrices tempus...</p>
                                    </div>
                                </li>
                                <li>
                                    <a href="#">
                                        <img class="rounded" src="img/70x70.png" alt="Image Alternative text" title="AMaze" />
                                    </a>
                                    <div class="thumb-list-item-caption">
                                        <p class="thumb-list-item-meta">7 minutes ago</p>
                                        <h4 class="thumb-list-item-title"><a href="#">Elizabeth Wallace</a></h4>
                                        <p class="thumb-list-item-desciption">Laoreet nullam sapien montes condimentum quis in...</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Archive</h4>
                            <ul class="icon-list list-category">
                                <li><a href="#"><i class="fa fa-angle-right"></i>July 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>June 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>May 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>April 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>March 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>February 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>January 2014</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-right"></i>December 2014</a>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Gallery</h4>
                            <div class="row row-no-gutter">
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="The Big Showoff-Take 2" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="Me with the Uke" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="Good job" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="Happy Bokeh Day" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="Spidy" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="4 Strokes of Fun" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="a dreamy jump" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="b and w camera" />
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <a class="hover-img" href="#">
                                        <img src="img/100x100.png" alt="Image Alternative text" title="sunny wood" />
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar-widget">
                            <h4>Facebook</h4>
                            <div class="fb-like-box" data-href="https://www.facebook.com/FacebookDevelopers" data-colorscheme="light" data-show-faces="1" data-header="1" data-show-border="1" data-width="233"></div>
                        </div>
                    </aside>
                </div>
                <div class="col-md-9">
                    <article class="post">
                        <header class="post-header">
                            <div class="fotorama" data-allowfullscreen="true">
                                <img src="img/1200x500.png" alt="Image Alternative text" title="196_365" />
                                <img src="img/1200x500.png" alt="Image Alternative text" title="196_365" />
                                <img src="img/1200x500.png" alt="Image Alternative text" title="196_365" />
                            </div>
                        </header>
                        <div class="post-inner">
                            <h4 class="post-title text-darken">Fames nibh at condimentum netus</h4>
                            <ul class="post-meta">
                                <li><i class="fa fa-calendar"></i><a href="#">17 October, 2014</a>
                                </li>
                                <li><i class="fa fa-user"></i><a href="#">Neil Davidson</a>
                                </li>
                                <li><i class="fa fa-tags"></i><a href="#">Lifestyle</a>
                                </li>
                                <li><i class="fa fa-comments"></i><a href="#">0 Comments</a>
                                </li>
                            </ul>
                            <p>Interdum iaculis euismod cursus hendrerit parturient suscipit erat felis taciti inceptos nec nisi luctus ac habitasse aptent lobortis etiam facilisis aliquet facilisi fames nunc imperdiet ut placerat auctor lectus suscipit</p>
                            <p>Massa sociosqu ante adipiscing hac ornare mus turpis habitant pretium scelerisque luctus at rutrum convallis dolor nibh aliquet imperdiet ante mollis duis ultrices magna volutpat molestie nam magna mollis potenti penatibus mauris etiam venenatis auctor montes sem ac consequat aliquet duis at penatibus vestibulum condimentum suspendisse aliquet sed nam nisl class mattis diam ad dapibus nam amet felis congue ultrices rhoncus egestas eros aptent lacus venenatis litora vehicula nunc elementum arcu ante litora eu tristique fusce metus sem feugiat etiam tortor aliquet diam sollicitudin dictumst placerat scelerisque massa luctus vitae aliquam egestas himenaeos nostra eleifend lobortis velit sociis eget duis</p>
                            <h5>Sociis habitant iaculis urna blandit iaculis</h5>
                            <p>Mattis nec felis odio viverra nunc non habitant nunc aptent natoque eget scelerisque fermentum euismod malesuada fusce integer habitant tempus nec quisque suspendisse neque iaculis hendrerit integer fringilla at placerat bibendum gravida malesuada porta luctus fusce ad suscipit porttitor molestie litora cubilia fringilla parturient dictum ad lobortis turpis accumsan ligula posuere ad facilisis ultricies cras donec torquent imperdiet habitasse dignissim convallis felis libero dictumst taciti placerat adipiscing ultrices lacinia velit cursus nisi per commodo lobortis vehicula urna netus aenean dictumst arcu imperdiet laoreet aliquam inceptos venenatis sagittis posuere sagittis eu pharetra ultricies natoque sit dis tristique nisi morbi varius himenaeos</p>
                            <h5>Dui urna a</h5>
                            <p>Molestie enim ut vivamus consequat arcu aliquam velit gravida maecenas dictumst mollis ultricies tellus tempor semper bibendum aptent aptent iaculis molestie imperdiet donec felis malesuada potenti facilisi eleifend porta himenaeos rutrum duis gravida donec ornare taciti cursus nam habitant est himenaeos eu taciti ad placerat praesent penatibus dui feugiat porttitor</p>
                            <p>Porttitor leo ad neque litora sem vestibulum dolor turpis tortor tortor tristique massa sed aliquam mollis eget conubia blandit lectus montes vel etiam ligula justo tempor feugiat ut feugiat volutpat libero praesent eget tortor iaculis cum nibh phasellus faucibus arcu inceptos varius vitae semper congue ridiculus nibh elit platea quisque</p>
                            <h5>Tempus platea nostra ac arcu accumsan sollicitudin condimentum sodales molestie</h5>
                            <p>Taciti cubilia libero congue nullam praesent non blandit nisl ultricies mi facilisis rhoncus hac urna commodo neque congue aenean fermentum aenean ridiculus mus fermentum nam lectus potenti ut curabitur posuere eleifend massa etiam per feugiat tempus tortor ultrices praesent integer maecenas montes nulla ullamcorper ac curabitur quis quis ut senectus gravida risus eleifend curae commodo feugiat iaculis elit elit eleifend fringilla placerat magnis suscipit nascetur natoque augue adipiscing amet magna porttitor class elementum montes netus tortor conubia felis ligula sagittis curae ullamcorper mi non fermentum ultrices mauris porttitor auctor lacus ornare turpis imperdiet aliquet odio natoque primis sollicitudin porttitor montes at lectus nam curae tellus ullamcorper lacus eros consectetur suspendisse aliquam dolor eros iaculis netus semper eu aptent semper ligula orci volutpat urna velit felis ridiculus parturient per rhoncus magnis dis laoreet venenatis quisque euismod nisi id pellentesque cum a vivamus risus maecenas viverra eleifend dui mi inceptos consectetur nascetur viverra neque blandit ultrices bibendum id consectetur tempor mollis cubilia phasellus metus natoque porttitor proin litora tortor ipsum quisque risus pulvinar congue pretium at semper cubilia blandit lacus fusce habitasse sit libero aliquam blandit interdum eget consectetur dictumst sapien habitasse fames volutpat lobortis ultricies senectus habitant id molestie id nisl</p>
                            <p>Ad et donec nisi fusce proin ad phasellus sem ad adipiscing placerat interdum potenti vel venenatis nisi turpis adipiscing hac habitant donec vehicula dui convallis porttitor vehicula hendrerit turpis vivamus</p>
                        </div>
                    </article>
                    <h2>Post Discussion</h2>
                    <!-- START COMMENTS -->
                    <ul class="comments-list">
                        <li>
                            <div class="article comment" inline_comment="comment">
                                <div class="comment-author">
                                    <img src="img/50x50.png" alt="Image Alternative text" title="Gamer Chick" />
                                </div>
                                <div class="comment-inner"><span class="comment-author-name">Sarah Slater</span>
                                    <p class="comment-content">Bibendum suscipit quis penatibus dignissim ullamcorper mattis scelerisque lacus elementum ac nulla in montes neque nibh diam aptent class</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 16</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="article comment" inline_comment="comment">
                                <div class="comment-author">
                                    <img src="img/50x50.png" alt="Image Alternative text" title="4 Strokes of Fun" />
                                </div>
                                <div class="comment-inner"><span class="comment-author-name">Blake Abraham</span>
                                    <p class="comment-content">Dis cras nostra scelerisque curabitur a etiam conubia libero conubia sociis felis arcu class volutpat potenti leo</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 24</a>
                                </div>
                            </div>
                            <ul>
                                <li>
                                    <div class="article comment" inline_comment="comment">
                                        <div class="comment-author">
                                            <img src="img/50x50.png" alt="Image Alternative text" title="Spidy" />
                                        </div>
                                        <div class="comment-inner"><span class="comment-author-name">Carol Blevins</span>
                                            <p class="comment-content">Porttitor id placerat suspendisse molestie sollicitudin ut suspendisse pellentesque nostra fringilla senectus cursus torquent praesent praesent orci integer laoreet adipiscing dignissim hendrerit fringilla porta inceptos magnis quis dui hendrerit ad</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 8</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <li>
                                <div class="article comment" inline_comment="comment">
                                    <div class="comment-author">
                                        <img src="img/50x50.png" alt="Image Alternative text" title="Good job" />
                                    </div>
                                    <div class="comment-inner"><span class="comment-author-name">Joseph Hudson</span>
                                        <p class="comment-content">Convallis turpis montes lacinia dui nisi aliquam rutrum quis faucibus potenti egestas nisi aenean dictum ligula blandit donec mi porttitor at</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 34</a>
                                    </div>
                                </div>
                                <ul>
                                    <li>
                                        <div class="article comment" inline_comment="comment">
                                            <div class="comment-author">
                                                <img src="img/50x50.png" alt="Image Alternative text" title="Ana 29" />
                                            </div>
                                            <div class="comment-inner"><span class="comment-author-name">Blake Abraham</span>
                                                <p class="comment-content">Praesent et justo sed platea est cras penatibus fermentum neque morbi vel nec sed imperdiet accumsan ultricies</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 34</a>
                                            </div>
                                        </div>
                                        <ul>
                                            <li>
                                                <div class="article comment" inline_comment="comment">
                                                    <div class="comment-author">
                                                        <img src="img/50x50.png" alt="Image Alternative text" title="Luca" />
                                                    </div>
                                                    <div class="comment-inner"><span class="comment-author-name">Frank Mills</span>
                                                        <p class="comment-content">Platea lacus mauris elementum gravida tincidunt magna ac habitant libero dapibus ante sapien gravida integer gravida etiam eu molestie habitasse tincidunt luctus integer ante eget penatibus hac tortor</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 35</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <li>
                                            <div class="article comment" inline_comment="comment">
                                                <div class="comment-author">
                                                    <img src="img/50x50.png" alt="Image Alternative text" title="Afro" />
                                                </div>
                                                <div class="comment-inner"><span class="comment-author-name">Richard Jones</span>
                                                    <p class="comment-content">Est pulvinar est malesuada habitant non vehicula fringilla netus cum ornare</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 12</a>
                                                </div>
                                            </div>
                                        </li>
                                </ul>
                                <li>
                                    <div class="article comment" inline_comment="comment">
                                        <div class="comment-author">
                                            <img src="img/50x50.png" alt="Image Alternative text" title="Chiara" />
                                        </div>
                                        <div class="comment-inner"><span class="comment-author-name">Minnie Aviles</span>
                                            <p class="comment-content">Per nisi semper et libero mauris eget cras facilisi aliquam vulputate</p><span class="comment-time">15 seconds ago</span><a class="comment-reply" href="#"><i class="fa fa-reply"></i> Reply</a><a class="comment-like" href="#"><i class="fa fa-heart"></i> 29</a>
                                        </div>
                                    </div>
                                    <ul></ul>
                    </ul>
                    <!-- END COMMENTS -->
                    <h3>Leave a Comment</h3>
                    <form>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input class="form-control" type="text" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>E-mail</label>
                                    <input class="form-control" type="text" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Website</label>
                                    <input class="form-control" type="text" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Comment</label>
                            <textarea class="form-control"></textarea>
                        </div>
                        <input class="btn btn-primary" type="submit" value="Leave a Comment" />
                    </form>
                </div>
            </div>
        </div>



        <div class="gap"></div>
     
	 <?php
		
		include "includes/files/footer.php";
		
		?>


        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script src="js/nicescroll.js"></script>
        <script src="js/dropit.js"></script>
        <script src="js/ionrangeslider.js"></script>
        <script src="js/icheck.js"></script>
        <script src="js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="js/typeahead.js"></script>
        <script src="js/card-payment.js"></script>
        <script src="js/magnific.js"></script>
        <script src="js/owl-carousel.js"></script>
        <script src="js/fitvids.js"></script>
        <script src="js/tweet.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/gridrotator.js"></script>
        <script src="js/custom.js"></script>
    </div>
</body>

</html>


