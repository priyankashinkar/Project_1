<!DOCTYPE HTML>
<html>

<?php
		
		include "includes/files/header_links.php";
		
		?>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        
		 <?php
		
		include "includes/files/page_header.php";
		
		?>

        <div class="container">
            <h1 class="page-title">Media Embedd</h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <aside class="sidebar-left">
                        <ul class="nav nav-pills nav-stacked nav-side mb30">

                            <li><a href="feature-typography.html">Typography</a>
                            </li>
                            <li><a href="feature-icons.html">Icons</a>
                            </li>
                            <li><a href="feature-forms.html">Forms</a>
                            </li>
                            <li><a href="feature-icon-effects.html">Icon Effects</a>
                            </li>
                            <li><a href="feature-elements.html">Elements</a>
                            </li>
                            <li><a href="feature-grid.html">Grid</a>
                            </li>
                            <li><a href="feature-hovers.html">Hover effects</a>
                            </li>
                            <li><a href="feature-lightbox.html">Lightbox</a>
                            </li>
                            <li class="active"><a href="feature-media.html">Media</a>
                            </li>
                        </ul>
                    </aside>
                </div>
                <div class="col-md-9">
                    <h4>Youtube</h4>
                    <iframe src="//www.youtube.com/embed/3JJv4TvURj4" frameborder="0" allowfullscreen></iframe>
                    <div class="gap"></div>
                    <h4>Vimeo</h4>
                    <iframe src="http://player.vimeo.com/video/70388552" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                    <div class="gap"></div>
                    <h4>Google Map</h4>
                    <div id="map-canvas" style="height: 300px"></div>
                    <div class="gap"></div>
                    <h4>Soundcloud Audio</h4>
                    <iframe src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F63121479" width="100%" height="166" scrolling="no" frameborder="no"></iframe>

                </div>
            </div>
        </div>



        <div class="gap"></div>
        
		<?php
		
		include "includes/files/footer.php";
		
		?>


        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script src="js/nicescroll.js"></script>
        <script src="js/dropit.js"></script>
        <script src="js/ionrangeslider.js"></script>
        <script src="js/icheck.js"></script>
        <script src="js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="js/typeahead.js"></script>
        <script src="js/card-payment.js"></script>
        <script src="js/magnific.js"></script>
        <script src="js/owl-carousel.js"></script>
        <script src="js/fitvids.js"></script>
        <script src="js/tweet.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/gridrotator.js"></script>
        <script src="js/custom.js"></script>
    </div>
</body>

</html>


